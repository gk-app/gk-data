--liquibase formatted sql
--changeset alex:201901_11

INSERT INTO award_category_info (id, name, description)
VALUES (0, 'TEAM', 'For command achievement'),
       (1, 'PERSONAL', 'For personal achievement'),
       (2, 'COMMUNITY', 'For achievement as a community member');
