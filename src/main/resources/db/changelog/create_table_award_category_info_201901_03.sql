--liquibase formatted sql
--changeset alex:201901_03

CREATE TABLE IF NOT EXISTS award_category_info (
    id          SMALLINT PRIMARY KEY,
    name        VARCHAR(10) NOT NULL UNIQUE,
    description VARCHAR(255)
);

--rollback DROP TABLE IF EXISTS award_category_info;
