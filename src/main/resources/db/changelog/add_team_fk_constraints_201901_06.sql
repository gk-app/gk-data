--liquibase formatted sql
--changeset alex:201901_06

ALTER TABLE team
    ADD COLUMN captain_id INTEGER UNIQUE REFERENCES player (player_id);
