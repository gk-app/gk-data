--liquibase formatted sql
--changeset alex:201906_03

ALTER TABLE player RENAME COLUMN residence TO court;

--rollback ALTER TABLE player RENAME COLUMN court TO residence;
