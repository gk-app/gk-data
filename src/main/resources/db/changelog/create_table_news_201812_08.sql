--liquibase formatted sql
--changeset alex:201812_08

CREATE TABLE IF NOT EXISTS news (
    news_id    SERIAL PRIMARY KEY,
    title      VARCHAR(50)        NOT NULL,
    content    VARCHAR(500)       NOT NULL,
    avatar     VARCHAR(255)       NOT NULL,
    thumbnail  VARCHAR(255)       NOT NULL,
    created_at DATE DEFAULT NOW() NOT NULL
);

--rollback DROP TABLE IF EXISTS news;
