--liquibase formatted sql
--changeset alex:201812_07

CREATE TABLE IF NOT EXISTS award (
    award_id    SERIAL PRIMARY KEY,
    name        VARCHAR(50)  NOT NULL UNIQUE,
    avatar      VARCHAR(255) NOT NULL,
    thumbnail   VARCHAR(255) NOT NULL,
    description VARCHAR(255)
);

--rollback DROP TABLE IF EXISTS award;
