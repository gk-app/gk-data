--liquibase formatted sql
--changeset alex:201901_01

CREATE TABLE IF NOT EXISTS draw_info (
    id          SMALLINT PRIMARY KEY,
    name        VARCHAR(10) NOT NULL UNIQUE,
    description VARCHAR(255)
);

--rollback DROP TABLE IF EXISTS draw_info;
