--liquibase formatted sql
--changeset alex:201812_03

CREATE TABLE IF NOT EXISTS team (
    team_id    SERIAL PRIMARY KEY,
    name       VARCHAR(20)        NOT NULL UNIQUE,
    avatar     VARCHAR(255)       NOT NULL,
    thumbnail  VARCHAR(255)       NOT NULL,
    created_at DATE DEFAULT NOW() NOT NULL
);

--rollback DROP TABLE IF EXISTS team;
