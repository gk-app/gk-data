--liquibase formatted sql
--changeset alex:201901_02

CREATE TABLE IF NOT EXISTS system_format_info (
    id          SMALLINT PRIMARY KEY,
    name        VARCHAR(10) NOT NULL UNIQUE,
    description VARCHAR(255)
);

--rollback DROP TABLE IF EXISTS system_format_info;
