--liquibase formatted sql
--changeset alex:201812_05

CREATE TABLE IF NOT EXISTS rules (
    rules_id      SERIAL PRIMARY KEY,
    name          VARCHAR(50)                                                     NOT NULL UNIQUE, -- its format will be tournament_name/tournament_date and some set of predefined rules
    team_format   INTEGER DEFAULT 5 CHECK (team_format >= 1 AND team_format <= 5) NOT NULL, -- 1, 2, 3, 4, 5
    time_format   VARCHAR(4)                                                      NOT NULL, -- 4x10, 2x20, 1x10
    attack_time   INTEGER                                                         NOT NULL,
    teams_limit   INTEGER DEFAULT 0                                               NOT NULL,
    created_at    DATE DEFAULT NOW()                                              NOT NULL,
    description   VARCHAR(255)
);

--rollback DROP TABLE IF EXISTS rules;
