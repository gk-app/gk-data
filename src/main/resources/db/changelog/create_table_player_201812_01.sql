--liquibase formatted sql
--changeset alex:201812_01

CREATE TABLE IF NOT EXISTS player (
  player_id     SERIAL PRIMARY KEY,
  nickname      VARCHAR(20)             NOT NULL UNIQUE,
  email         VARCHAR(50)             NOT NULL UNIQUE,
  avatar        VARCHAR(255)            NOT NULL,
  thumbnail     VARCHAR(255)            NOT NULL,
  fname         VARCHAR(20)             NOT NULL,
  lname         VARCHAR(20)             NOT NULL,
  is_male       BOOLEAN                 NOT NULL,
  date_of_birth DATE                    NOT NULL,
  last_seen     TIMESTAMP DEFAULT now() NOT NULL,
  created_at    DATE DEFAULT NOW()      NOT NULL,
  CHECK (date_of_birth >= '1920-01-01' AND date_of_birth <= '2010-01-01')
);

--rollback DROP TABLE IF EXISTS player;
