--liquibase formatted sql
--changeset alex:201812_02

CREATE TABLE IF NOT EXISTS court (
    court_id    SERIAL PRIMARY KEY,
    name        VARCHAR(50)                                                NOT NULL,
    avatar      VARCHAR(255)                                               NOT NULL,
    thumbnail   VARCHAR(255)                                               NOT NULL,
    latitude    DECIMAL(9, 6)                                              NOT NULL,
    longitude   DECIMAL(9, 6)                                              NOT NULL,
    rating      INTEGER DEFAULT 5 CHECK (rating >= 1 AND rating <= 10)     NOT NULL,
    created_at  DATE DEFAULT NOW()                                         NOT NULL,
    description VARCHAR(255)
);

--rollback DROP TABLE IF EXISTS court;
