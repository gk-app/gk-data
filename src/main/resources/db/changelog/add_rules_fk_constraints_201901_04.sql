--liquibase formatted sql
--changeset alex:201901_04

ALTER TABLE rules
    ADD COLUMN draw_id SMALLINT REFERENCES draw_info (id);
ALTER TABLE rules
    ADD COLUMN system_format_id SMALLINT REFERENCES system_format_info (id);
