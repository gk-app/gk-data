--liquibase formatted sql
--changeset alex:201901_08

ALTER TABLE award
    ADD COLUMN award_category INTEGER REFERENCES award_category_info (id);
