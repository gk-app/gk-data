--liquibase formatted sql
--changeset alex:201901_05

ALTER TABLE player
    ADD COLUMN residence INTEGER REFERENCES court (court_id);
ALTER TABLE player
    ADD COLUMN team INTEGER REFERENCES team (team_id);
