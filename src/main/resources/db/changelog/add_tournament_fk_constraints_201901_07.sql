--liquibase formatted sql
--changeset alex:201901_07

ALTER TABLE tournament
    ADD COLUMN place INTEGER NOT NULL REFERENCES court (court_id);
ALTER TABLE tournament
    ADD COLUMN organizer INTEGER NOT NULL REFERENCES player (player_id);
ALTER TABLE tournament
    ADD COLUMN rules INTEGER NOT NULL REFERENCES rules (rules_id);
ALTER TABLE tournament
    ADD COLUMN coorganizer INTEGER REFERENCES player (player_id);
ALTER TABLE tournament
    ADD COLUMN judge_one INTEGER REFERENCES player (player_id);
ALTER TABLE tournament
    ADD COLUMN judge_two INTEGER REFERENCES player (player_id);
