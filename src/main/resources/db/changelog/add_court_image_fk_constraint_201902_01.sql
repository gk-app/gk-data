--liquibase formatted sql
--changeset alex:201902_01

ALTER TABLE court_image
    ADD COLUMN court_id INTEGER REFERENCES court (court_id);
