--liquibase formatted sql
--changeset alex:201812_04

CREATE TABLE IF NOT EXISTS tournament (
    tournament_id SERIAL PRIMARY KEY,
    name          VARCHAR(20)        NOT NULL,
    avatar        VARCHAR(255)       NOT NULL,
    thumbnail     VARCHAR(255)       NOT NULL,
    date          TIMESTAMP          NOT NULL,
    created_at    DATE DEFAULT NOW() NOT NULL,
    CHECK (date > TIMESTAMP 'now')
);

--rollback DROP TABLE IF EXISTS tournament;
