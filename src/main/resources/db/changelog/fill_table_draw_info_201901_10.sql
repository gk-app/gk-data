--liquibase formatted sql
--changeset alex:201901_10

INSERT INTO draw_info (id, name, description)
VALUES (0, 'MANUAL', 'Performed by organizermanually'),
       (1, 'AUTOMATED', 'Performed by system'),
       (2, 'MIXED', 'Seeded teams are chosen by organizer');
