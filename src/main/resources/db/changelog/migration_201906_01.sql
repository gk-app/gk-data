--liquibase formatted sql
--changeset alex:201906_01

ALTER TABLE award ALTER COLUMN thumbnail DROP NOT NULL;
ALTER TABLE court ALTER COLUMN thumbnail DROP NOT NULL;
ALTER TABLE news ALTER COLUMN thumbnail DROP NOT NULL;
ALTER TABLE player ALTER COLUMN thumbnail DROP NOT NULL;
ALTER TABLE team ALTER COLUMN thumbnail DROP NOT NULL;
ALTER TABLE tournament ALTER COLUMN thumbnail DROP NOT NULL;

--rollback ALTER TABLE award ALTER COLUMN thumbnail SET NOT NULL;
--rollback ALTER TABLE court ALTER COLUMN thumbnail SET NOT NULL;
--rollback ALTER TABLE news ALTER COLUMN thumbnail SET NOT NULL;
--rollback ALTER TABLE player ALTER COLUMN thumbnail SET NOT NULL;
--rollback ALTER TABLE team ALTER COLUMN thumbnail SET NOT NULL;
--rollback ALTER TABLE tournament ALTER COLUMN thumbnail SET NOT NULL;
