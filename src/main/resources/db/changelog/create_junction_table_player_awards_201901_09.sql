--liquibase formatted sql
--changeset alex:201901_09

CREATE TABLE IF NOT EXISTS player_awards (
    player_id INTEGER REFERENCES player (player_id) NOT NULL,
    award_id  INTEGER REFERENCES award (award_id) NOT NULL
);

--rollback DROP TABLE IF EXISTS player_awards;
