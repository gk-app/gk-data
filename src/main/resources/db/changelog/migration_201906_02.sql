--liquibase formatted sql
--changeset alex:201906_02

ALTER TABLE award ALTER COLUMN avatar DROP NOT NULL;
ALTER TABLE court ALTER COLUMN avatar DROP NOT NULL;
ALTER TABLE news ALTER COLUMN avatar DROP NOT NULL;
ALTER TABLE player ALTER COLUMN avatar DROP NOT NULL;
ALTER TABLE team ALTER COLUMN avatar DROP NOT NULL;
ALTER TABLE tournament ALTER COLUMN avatar DROP NOT NULL;

--rollback ALTER TABLE award ALTER COLUMN avatar SET NOT NULL;
--rollback ALTER TABLE court ALTER COLUMN avatar SET NOT NULL;
--rollback ALTER TABLE news ALTER COLUMN avatar SET NOT NULL;
--rollback ALTER TABLE player ALTER COLUMN avatar SET NOT NULL;
--rollback ALTER TABLE team ALTER COLUMN avatar SET NOT NULL;
--rollback ALTER TABLE tournament ALTER COLUMN avatar SET NOT NULL;
