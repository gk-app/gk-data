--liquibase formatted sql
--changeset alex:201901_12

INSERT INTO system_format_info (id, name, description)
VALUES (0, 'KNOCKOUT', 'On each stage one team is eliminated'),
       (1, 'GROUP', 'Teams play with each other and the most successful team wins'),
       (2, 'MIXED', 'Consists of a group stage first and followed by a knockout stage');
