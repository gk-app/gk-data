--liquibase formatted sql
--changeset alex:202102_01

ALTER TABLE player DROP CONSTRAINT player_date_of_birth_check;
ALTER TABLE player ADD CONSTRAINT player_date_of_birth_check
    CHECK (date_of_birth >= NOW() - INTERVAL '100 YEAR' AND date_of_birth <= NOW() - INTERVAL '10 YEAR');

