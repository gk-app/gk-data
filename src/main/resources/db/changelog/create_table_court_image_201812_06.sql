--liquibase formatted sql
--changeset alex:201812_06

CREATE TABLE IF NOT EXISTS court_image (
    court_image_id  SERIAL PRIMARY KEY,
    link      VARCHAR(255) NOT NULL,
    thumbnail VARCHAR(255)
);

--rollback DROP TABLE IF EXISTS court_image;
