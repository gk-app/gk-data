package ua.in.gdekatka.gkdata.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@Profile({"prod", "dev"})
public class WebConfig implements WebMvcConfigurer {

    @Value("${gk-props.gk-ui-admin.origin}")
    private String gkUiAdminOrigin;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins(gkUiAdminOrigin)
                .allowedMethods("*");
    }
}
