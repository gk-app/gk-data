package ua.in.gdekatka.gkdata.config;

/**
 * Application constants.
 */
public final class Constants {

    private Constants() { }

    public static final String REST_PATH_PREFIX = "/rest";
}
