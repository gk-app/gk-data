package ua.in.gdekatka.gkdata.config;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JacksonConfig {

    // Jackson module to better work with Hibernate Lazy Loading
    @Bean
    protected Module module() {
        return new Hibernate5Module();
    }
}
