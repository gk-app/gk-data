package ua.in.gdekatka.gkdata.service;

import ua.in.gdekatka.gkdata.model.Player;

import java.util.List;
import java.util.Optional;

public interface PlayerService {

    Player save(Player player);

    List<Player> findAll();

    Optional<Player> findOne(Long playerId);

    void delete(Long playerId);
}
