package ua.in.gdekatka.gkdata.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.in.gdekatka.gkdata.model.News;
import ua.in.gdekatka.gkdata.repository.NewsRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class NewsServiceImpl implements NewsService {

    private final Logger log = LoggerFactory.getLogger(NewsServiceImpl.class);

    private final NewsRepository newsRepository;

    public NewsServiceImpl(NewsRepository newsRepository) {
        this.newsRepository = newsRepository;
    }

    @Override
    public News save(News news) {
        log.debug("Request to save News : {}", news);
        return newsRepository.save(news);
    }

    @Override
    @Transactional(readOnly = true)
    public List<News> findAll() {
        log.debug("Request to get all Newss");
        return newsRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<News> findOne(Long newsId) {
        log.debug("Request to get News : {}", newsId);
        return newsRepository.findById(newsId);
    }

    @Override
    public void delete(Long newsId) {
        log.debug("Request to delete News : {}", newsId);
        newsRepository.deleteById(newsId);
    }

}
