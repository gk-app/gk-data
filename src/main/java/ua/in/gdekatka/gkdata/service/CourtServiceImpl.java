package ua.in.gdekatka.gkdata.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.in.gdekatka.gkdata.model.Court;
import ua.in.gdekatka.gkdata.repository.CourtRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CourtServiceImpl implements CourtService {

    private final Logger log = LoggerFactory.getLogger(CourtServiceImpl.class);

    private final CourtRepository courtRepository;

    public CourtServiceImpl(CourtRepository courtRepository) {
        this.courtRepository = courtRepository;
    }

    @Override
    public Court save(Court court) {
        log.debug("Request to save Court : {}", court);
        return courtRepository.save(court);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Court> findAll() {
        log.debug("Request to get all Courts");
        return courtRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Court> findOne(Long courtId) {
        log.debug("Request to get Court : {}", courtId);
        return courtRepository.findById(courtId);
    }

    @Override
    public void delete(Long courtId) {
        log.debug("Request to delete Court : {}", courtId);
        courtRepository.deleteById(courtId);
    }
}
