package ua.in.gdekatka.gkdata.service;

import ua.in.gdekatka.gkdata.model.Award;

import java.util.List;
import java.util.Optional;

public interface AwardService {

    Award save(Award award);

    List<Award> findAll();


    Optional<Award> findOne(Long awardId);

    void delete(Long awardId);
}
