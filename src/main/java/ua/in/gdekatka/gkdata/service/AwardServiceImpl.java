package ua.in.gdekatka.gkdata.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.in.gdekatka.gkdata.model.Award;
import ua.in.gdekatka.gkdata.repository.AwardRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class AwardServiceImpl implements AwardService {

    private final Logger log = LoggerFactory.getLogger(AwardServiceImpl.class);

    private final AwardRepository awardRepository;

    public AwardServiceImpl(AwardRepository awardRepository) {
        this.awardRepository = awardRepository;
    }

    @Override
    public Award save(Award award) {
        log.debug("Request to save Award : {}", award);
        return awardRepository.save(award);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Award> findAll() {
        log.debug("Request to get all Awards");
        return awardRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Award> findOne(Long awardId) {
        log.debug("Request to get Award : {}", awardId);
        return awardRepository.findById(awardId);
    }

    @Override
    public void delete(Long awardId) {
        log.debug("Request to delete Award : {}", awardId);
        awardRepository.deleteById(awardId);
    }

}
