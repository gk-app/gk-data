package ua.in.gdekatka.gkdata.service;

import ua.in.gdekatka.gkdata.model.Tournament;

import java.util.List;
import java.util.Optional;

public interface TournamentService {

    Tournament save(Tournament tournament);

    List<Tournament> findAll();

    Optional<Tournament> findOne(Long tournamentId);

    void delete(Long tournamentId);
}
