package ua.in.gdekatka.gkdata.service;

import ua.in.gdekatka.gkdata.model.Rules;

import java.util.List;
import java.util.Optional;

public interface RulesService {

    Rules save(Rules rules);

    List<Rules> findAll();

    Optional<Rules> findOne(Long rulesId);

    void delete(Long rulesId);
}
