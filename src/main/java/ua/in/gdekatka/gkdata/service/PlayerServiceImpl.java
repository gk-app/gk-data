package ua.in.gdekatka.gkdata.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.in.gdekatka.gkdata.model.Player;
import ua.in.gdekatka.gkdata.repository.PlayerRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PlayerServiceImpl implements PlayerService {

    private final Logger log = LoggerFactory.getLogger(PlayerServiceImpl.class);

    private final PlayerRepository playerRepository;

    public PlayerServiceImpl(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @Override
    public Player save(Player player) {
        log.debug("Request to save Player : {}", player);
        return playerRepository.save(player);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Player> findAll() {
        log.debug("Request to get all Players");
        return playerRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Player> findOne(Long playerId) {
        log.debug("Request to get Player : {}", playerId);
        return playerRepository.findById(playerId);
    }

    @Override
    public void delete(Long playerId) {
        log.debug("Request to delete Player : {}", playerId);
        playerRepository.deleteById(playerId);
    }

}
