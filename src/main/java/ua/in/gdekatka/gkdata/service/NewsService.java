package ua.in.gdekatka.gkdata.service;

import ua.in.gdekatka.gkdata.model.News;

import java.util.List;
import java.util.Optional;

public interface NewsService {

    News save(News news);

    List<News> findAll();


    Optional<News> findOne(Long newsId);

    void delete(Long newsId);
}
