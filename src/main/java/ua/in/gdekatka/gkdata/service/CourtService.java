package ua.in.gdekatka.gkdata.service;

import ua.in.gdekatka.gkdata.model.Court;

import java.util.List;
import java.util.Optional;

public interface CourtService {

    Court save(Court court);

    List<Court> findAll();

    Optional<Court> findOne(Long courtId);

    void delete(Long courtId);
}
