package ua.in.gdekatka.gkdata.service;

import ua.in.gdekatka.gkdata.model.Team;

import java.util.List;
import java.util.Optional;

public interface TeamService {

    Team save(Team team);

    List<Team> findAll();

    Optional<Team> findOne(Long teamId);

    void delete(Long teamId);
}
