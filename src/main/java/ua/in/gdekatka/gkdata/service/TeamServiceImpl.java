package ua.in.gdekatka.gkdata.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.in.gdekatka.gkdata.model.Team;
import ua.in.gdekatka.gkdata.repository.TeamRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TeamServiceImpl implements TeamService {

    private final Logger log = LoggerFactory.getLogger(TeamServiceImpl.class);

    private final TeamRepository teamRepository;

    public TeamServiceImpl(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Override
    public Team save(Team team) {
        log.debug("Request to save Team : {}", team);
        return teamRepository.save(team);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Team> findAll() {
        log.debug("Request to get all Teams");
        return teamRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Team> findOne(Long teamId) {
        log.debug("Request to get Team : {}", teamId);
        return teamRepository.findById(teamId);
    }

    @Override
    public void delete(Long teamId) {
        log.debug("Request to delete Team : {}", teamId);
        teamRepository.deleteById(teamId);
    }
}
