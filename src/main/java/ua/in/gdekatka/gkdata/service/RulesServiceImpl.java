package ua.in.gdekatka.gkdata.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.in.gdekatka.gkdata.model.Rules;
import ua.in.gdekatka.gkdata.repository.RulesRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RulesServiceImpl implements RulesService {


    private final Logger log = LoggerFactory.getLogger(RulesServiceImpl.class);

    private final RulesRepository rulesRepository;

    public RulesServiceImpl(RulesRepository rulesRepository) {
        this.rulesRepository = rulesRepository;
    }

    @Override
    public Rules save(Rules rules) {
        log.debug("Request to save Rules : {}", rules);
        return rulesRepository.save(rules);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Rules> findAll() {
        log.debug("Request to get all the Rules");
        return rulesRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Rules> findOne(Long rulesId) {
        log.debug("Request to get Rules : {}", rulesId);
        return rulesRepository.findById(rulesId);
    }

    @Override
    public void delete(Long rulesId) {
        log.debug("Request to delete Rules : {}", rulesId);
        rulesRepository.deleteById(rulesId);
    }
}
