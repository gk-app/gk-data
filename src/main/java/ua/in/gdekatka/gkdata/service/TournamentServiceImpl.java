package ua.in.gdekatka.gkdata.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.in.gdekatka.gkdata.model.Tournament;
import ua.in.gdekatka.gkdata.repository.TournamentRepository;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TournamentServiceImpl implements TournamentService {

    private final Logger log = LoggerFactory.getLogger(TournamentServiceImpl.class);

    private final TournamentRepository tournamentRepository;

    public TournamentServiceImpl(TournamentRepository tournamentRepository) {
        this.tournamentRepository = tournamentRepository;
    }

    @Override
    public Tournament save(Tournament tournament) {
        log.debug("Request to save Tournament : {}", tournament);
        return tournamentRepository.save(tournament);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Tournament> findAll() {
        log.debug("Request to get all Tournaments");
        return tournamentRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Tournament> findOne(Long tournamentId) {
        log.debug("Request to get Tournament : {}", tournamentId);
        return tournamentRepository.findById(tournamentId);
    }

    @Override
    public void delete(Long tournamentId) {
        log.debug("Request to delete Tournament : {}", tournamentId);
        tournamentRepository.deleteById(tournamentId);
    }
}
