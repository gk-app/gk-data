package ua.in.gdekatka.gkdata.controller.graphql;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.in.gdekatka.gkdata.model.Award;
import ua.in.gdekatka.gkdata.model.Court;
import ua.in.gdekatka.gkdata.model.Player;
import ua.in.gdekatka.gkdata.model.Team;
import ua.in.gdekatka.gkdata.repository.AwardRepository;
import ua.in.gdekatka.gkdata.repository.CourtRepository;
import ua.in.gdekatka.gkdata.repository.TeamRepository;

import java.util.List;

@Component
public class PlayerResolver implements GraphQLResolver<Player> {

    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private CourtRepository courtRepository;
    @Autowired
    private AwardRepository awardRepository;

    public Team getTeam(Player player) {
        return teamRepository.findTeamOfPlayer(player.getPlayerId());
    }

    public Court getCourt(Player player) {
        return courtRepository.findCourtOfPlayer(player.getPlayerId());
    }

    public List<Award> getAwards(Player player) {
        return awardRepository.findAwardsOfPlayer(player.getPlayerId());
    }
}