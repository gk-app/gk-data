package ua.in.gdekatka.gkdata.controller.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.in.gdekatka.gkdata.config.Constants;
import ua.in.gdekatka.gkdata.model.Award;
import ua.in.gdekatka.gkdata.service.AwardService;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Award.
 */
@RestController
@RequestMapping(Constants.REST_PATH_PREFIX)
public class AwardController {

    private final Logger log = LoggerFactory.getLogger(AwardController.class);

    private final AwardService awardService;

    public AwardController(AwardService awardService) {
        this.awardService = awardService;
    }

    /**
     * POST  /awards : Create a new Award.
     *
     * @param award the Award to create
     * @return the ResponseEntity with status 201 (Created) and with body the new Award, or with status 400 (Bad Request) if the Award has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/awards")
    public ResponseEntity<Award> createAward(@Valid @RequestBody Award award) throws URISyntaxException {
        log.debug("REST request to save Award : {}", award);
        if (award.getAwardId() != null) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        Award result = awardService.save(award);

        return ResponseEntity.created(new URI(Constants.REST_PATH_PREFIX + "/awards/" + result.getAwardId()))
            .body(result);
    }

    /**
     * PUT  /awards : Updates an existing Award.
     *
     * @param award the Award to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated Award,
     * or with status 400 (Bad Request) if the Award is not valid,
     * or with status 500 (Internal Server Error) if the Award couldn't be updated
     */
    @PutMapping("/awards/{awardId}")
    public ResponseEntity<Award> updateAward(@Valid @RequestBody Award award, @PathVariable Long awardId) {
        log.debug("REST request to update Award : {}", award);
        if (award.getAwardId() == null || !award.getAwardId().equals(awardId)) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        Award result = awardService.save(award);

        return ResponseEntity.ok()
            .body(result);
    }

    /**
     * GET  /awards : get all the Award.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of Award in body
     */
    @GetMapping("/awards")
    public List<Award> getAllAwards() {
        log.debug("REST request to get all Awards");

        return awardService.findAll();
    }

    /**
     * GET  /awards/:awardId : get the Award by awardId.
     *
     * @param awardId the awardId of the Award to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the Award, or with status 404 (Not Found)
     */
    @GetMapping("/awards/{awardId}")
    public ResponseEntity<Award> getAward(@PathVariable Long awardId) {
        log.debug("REST request to get Award : {}", awardId);
        Optional<Award> award = awardService.findOne(awardId);

        return award.map(response -> ResponseEntity.ok().body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /awards/:awardId : delete the Award by awardId.
     *
     * @param awardId the awardId of the Award to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/awards/{awardId}")
    public ResponseEntity<Void> deleteAward(@PathVariable Long awardId) {
        log.debug("REST request to delete Award : {}", awardId);
        awardService.delete(awardId);

        return ResponseEntity.ok().build();
    }
}
