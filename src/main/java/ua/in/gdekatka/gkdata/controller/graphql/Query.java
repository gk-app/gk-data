package ua.in.gdekatka.gkdata.controller.graphql;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import graphql.GraphQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.in.gdekatka.gkdata.model.Player;
import ua.in.gdekatka.gkdata.model.Team;
import ua.in.gdekatka.gkdata.repository.PlayerRepository;
import ua.in.gdekatka.gkdata.repository.TeamRepository;

import java.util.List;

@Service
public class Query implements GraphQLQueryResolver {

    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private TeamRepository teamRepository;

    public List<Player> getPlayers() {
        List<Player> playerList = playerRepository.findAll();
        playerList.forEach(player -> player.setExperience(player.calculateExperience()));

        return playerList;
    }

    public Player getPlayer(Long playerId) {
        return playerRepository.findById(playerId).orElseThrow(() -> new GraphQLException("Player not found"));
    }

    public List<Team> getTeams() {
        return teamRepository.findAll();
    }

    public Team getTeam(Long teamId) {
        return teamRepository.findById(teamId).orElseThrow(() -> new GraphQLException("Team not found"));
    }
}