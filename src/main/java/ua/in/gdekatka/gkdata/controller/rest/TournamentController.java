package ua.in.gdekatka.gkdata.controller.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.in.gdekatka.gkdata.config.Constants;
import ua.in.gdekatka.gkdata.model.Tournament;
import ua.in.gdekatka.gkdata.service.TournamentService;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Tournament.
 */
@RestController
@RequestMapping(Constants.REST_PATH_PREFIX)
public class TournamentController {

    private final Logger log = LoggerFactory.getLogger(TournamentController.class);

    private final TournamentService tournamentService;

    public TournamentController(TournamentService tournamentService) {
        this.tournamentService = tournamentService;
    }

    /**
     * POST  /tournaments : Create a new Tournament.
     *
     * @param tournament the Tournament to create
     * @return the ResponseEntity with status 201 (Created) and with body the new Tournament, or with status 400 (Bad Request) if the Tournament has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tournaments")
    public ResponseEntity<Tournament> createTournament(@Valid @RequestBody Tournament tournament) throws URISyntaxException {
        log.debug("REST request to save Tournament : {}", tournament);
        if (tournament.getTournamentId() != null) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        Tournament result = tournamentService.save(tournament);

        return ResponseEntity.created(new URI(Constants.REST_PATH_PREFIX + "/tournaments/" + result.getTournamentId()))
            .body(result);
    }

    /**
     * PUT  /tournaments : Updates an existing Tournament.
     *
     * @param tournament the Tournament to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated Tournament,
     * or with status 400 (Bad Request) if the Tournament is not valid,
     * or with status 500 (Internal Server Error) if the Tournament couldn't be updated
     */
    @PutMapping("/tournaments/{tournamentId}")
    public ResponseEntity<Tournament> updateTournament(@Valid @RequestBody Tournament tournament, @PathVariable Long tournamentId) {
        log.debug("REST request to update Tournament : {}", tournament);
        if (tournament.getTournamentId() == null || !tournament.getTournamentId().equals(tournamentId)) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        Tournament result = tournamentService.save(tournament);

        return ResponseEntity.ok()
            .body(result);
    }

    /**
     * GET  /tournaments : get all the Tournament.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of Tournament in body
     */
    @GetMapping("/tournaments")
    public List<Tournament> getAllTournaments() {
        log.debug("REST request to get all Tournaments");

        return tournamentService.findAll();
    }

    /**
     * GET  /tournaments/:tournamentId : get the Tournament by tournamentId.
     *
     * @param tournamentId the tournamentId of the Tournament to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the Tournament, or with status 404 (Not Found)
     */
    @GetMapping("/tournaments/{tournamentId}")
    public ResponseEntity<Tournament> getTournament(@PathVariable Long tournamentId) {
        log.debug("REST request to get Tournament : {}", tournamentId);
        Optional<Tournament> tournament = tournamentService.findOne(tournamentId);

        return tournament.map(response -> ResponseEntity.ok().body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /tournaments/:tournamentId : delete the Tournament by tournamentId.
     *
     * @param tournamentId the tournamentId of the Tournament to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tournaments/{tournamentId}")
    public ResponseEntity<Void> deleteTournament(@PathVariable Long tournamentId) {
        log.debug("REST request to delete Tournament : {}", tournamentId);
        tournamentService.delete(tournamentId);

        return ResponseEntity.ok().build();
    }
}
