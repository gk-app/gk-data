package ua.in.gdekatka.gkdata.controller.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.in.gdekatka.gkdata.config.Constants;
import ua.in.gdekatka.gkdata.model.Team;
import ua.in.gdekatka.gkdata.service.TeamService;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Team.
 */
@RestController
@RequestMapping(Constants.REST_PATH_PREFIX)
public class TeamController {

    private final Logger log = LoggerFactory.getLogger(TeamController.class);

    private final TeamService teamService;

    public TeamController(TeamService teamService) {
        this.teamService = teamService;
    }

    /**
     * POST  /teams : Create a new Team.
     *
     * @param team the Team to create
     * @return the ResponseEntity with status 201 (Created) and with body the new Team, or with status 400 (Bad Request) if the Team has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/teams")
    public ResponseEntity<Team> createTeam(@Valid @RequestBody Team team) throws URISyntaxException {
        log.debug("REST request to save Team : {}", team);
        if (team.getTeamId() != null) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        Team result = teamService.save(team);

        return ResponseEntity.created(new URI(Constants.REST_PATH_PREFIX + "/teams/" + result.getTeamId()))
            .body(result);
    }

    /**
     * PUT  /teams : Updates an existing Team.
     *
     * @param team the Team to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated Team,
     * or with status 400 (Bad Request) if the Team is not valid,
     * or with status 500 (Internal Server Error) if the Team couldn't be updated
     */
    @PutMapping("/teams/{teamId}")
    public ResponseEntity<Team> updateTeam(@Valid @RequestBody Team team, @PathVariable Long teamId) {
        log.debug("REST request to update Team : {}", team);
        if (team.getTeamId() == null || !team.getTeamId().equals(teamId)) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        Team result = teamService.save(team);

        return ResponseEntity.ok()
            .body(result);
    }

    /**
     * GET  /teams : get all the Team.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of Team in body
     */
    @GetMapping("/teams")
    public List<Team> getAllTeams() {
        log.debug("REST request to get all Teams");

        return teamService.findAll();
    }

    /**
     * GET  /teams/:teamId : get the Team by teamId.
     *
     * @param teamId the teamId of the Team to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the Team, or with status 404 (Not Found)
     */
    @GetMapping("/teams/{teamId}")
    public ResponseEntity<Team> getTeam(@PathVariable Long teamId) {
        log.debug("REST request to get Team : {}", teamId);
        Optional<Team> team = teamService.findOne(teamId);

        return team.map(response -> ResponseEntity.ok().body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /teams/:teamId : delete the Team by teamId.
     *
     * @param teamId the teamId of the Team to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/teams/{teamId}")
    public ResponseEntity<Void> deleteTeam(@PathVariable Long teamId) {
        log.debug("REST request to delete Team : {}", teamId);
        teamService.delete(teamId);

        return ResponseEntity.ok().build();
    }
}
