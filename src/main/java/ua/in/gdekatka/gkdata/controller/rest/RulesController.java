package ua.in.gdekatka.gkdata.controller.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.in.gdekatka.gkdata.config.Constants;
import ua.in.gdekatka.gkdata.model.Rules;
import ua.in.gdekatka.gkdata.service.RulesService;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Rules.
 */
@RestController
@RequestMapping(Constants.REST_PATH_PREFIX)
public class RulesController {

    private final Logger log = LoggerFactory.getLogger(RulesController.class);

    private final RulesService rulesService;

    public RulesController(RulesService rulesService) {
        this.rulesService = rulesService;
    }

    /**
     * POST  /rules : Create a new Rules.
     *
     * @param rules the Rules to create
     * @return the ResponseEntity with status 201 (Created) and with body the new Rules, or with status 400 (Bad Request) if the Rules has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/rules")
    public ResponseEntity<Rules> createRules(@Valid @RequestBody Rules rules) throws URISyntaxException {
        log.debug("REST request to save the Rules : {}", rules);
        if (rules.getRulesId() != null) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        Rules result = rulesService.save(rules);

        return ResponseEntity.created(new URI(Constants.REST_PATH_PREFIX + "/rules/" + result.getRulesId()))
            .body(result);
    }

    /**
     * PUT  /rules : Updates an existing Rules.
     *
     * @param rules the Rules to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated Rules,
     * or with status 400 (Bad Request) if the Rules are not valid,
     * or with status 500 (Internal Server Error) if the Rules couldn't be updated
     */
    @PutMapping("/rules/{rulesId}")
    public ResponseEntity<Rules> updateRules(@Valid @RequestBody Rules rules, @PathVariable Long rulesId) {
        log.debug("REST request to update Rules : {}", rules);
        if (rules.getRulesId() == null || !rules.getRulesId().equals(rulesId)) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        Rules result = rulesService.save(rules);

        return ResponseEntity.ok()
            .body(result);
    }

    /**
     * GET  /rules : get all the Rules.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of Rules in body
     */
    @GetMapping("/rules")
    public List<Rules> getAllRules() {
        log.debug("REST request to get all Rules");

        return rulesService.findAll();
    }

    /**
     * GET  /rules/:rulesId : get the Rules by rulesId.
     *
     * @param rulesId the rulesId of the Rules to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the Rules, or with status 404 (Not Found)
     */
    @GetMapping("/rules/{rulesId}")
    public ResponseEntity<Rules> getRules(@PathVariable Long rulesId) {
        log.debug("REST request to get Rules : {}", rulesId);
        Optional<Rules> rules = rulesService.findOne(rulesId);

        return rules.map(response -> ResponseEntity.ok().body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /rules/:rulesId : delete the Rules by rulesId.
     *
     * @param rulesId the rulesId of the Rules to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/rules/{rulesId}")
    public ResponseEntity<Void> deleteRules(@PathVariable Long rulesId) {
        log.debug("REST request to delete Rules : {}", rulesId);
        rulesService.delete(rulesId);

        return ResponseEntity.ok().build();
    }

}
