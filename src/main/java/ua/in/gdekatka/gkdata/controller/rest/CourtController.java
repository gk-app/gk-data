package ua.in.gdekatka.gkdata.controller.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.in.gdekatka.gkdata.config.Constants;
import ua.in.gdekatka.gkdata.model.Court;
import ua.in.gdekatka.gkdata.service.CourtService;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Court.
 */
@RestController
@RequestMapping(Constants.REST_PATH_PREFIX)
public class CourtController {

    private final Logger log = LoggerFactory.getLogger(CourtController.class);

    private final CourtService courtService;

    public CourtController(CourtService courtService) {
        this.courtService = courtService;
    }

    /**
     * POST  /courts : Create a new Court.
     *
     * @param court the Court to create
     * @return the ResponseEntity with status 201 (Created) and with body the new Court, or with status 400 (Bad Request) if the Court has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/courts")
    public ResponseEntity<Court> createCourt(@Valid @RequestBody Court court) throws URISyntaxException {
        log.debug("REST request to save Court : {}", court);
        if (court.getCourtId() != null) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        Court result = courtService.save(court);

        return ResponseEntity.created(new URI(Constants.REST_PATH_PREFIX + "/courts/" + result.getCourtId()))
            .body(result);
    }

    /**
     * PUT  /courts : Updates an existing Court.
     *
     * @param court the Court to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated Court,
     * or with status 400 (Bad Request) if the Court is not valid,
     * or with status 500 (Internal Server Error) if the Court couldn't be updated
     */
    @PutMapping("/courts/{courtId}")
    public ResponseEntity<Court> updateCourt(@Valid @RequestBody Court court, @PathVariable Long courtId) {
        log.debug("REST request to update Court : {}", court);
        if (court.getCourtId() == null || !court.getCourtId().equals(courtId)) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        Court result = courtService.save(court);

        return ResponseEntity.ok()
            .body(result);
    }

    /**
     * GET  /courts : get all the Court.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of Court in body
     */
    @GetMapping("/courts")
    public List<Court> getAllCourts() {
        log.debug("REST request to get all Courts");

        return courtService.findAll();
    }

    /**
     * GET  /courts/:courtId : get the Court by courtId.
     *
     * @param courtId the courtId of the Court to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the Court, or with status 404 (Not Found)
     */
    @GetMapping("/courts/{courtId}")
    public ResponseEntity<Court> getCourt(@PathVariable Long courtId) {
        log.debug("REST request to get Court : {}", courtId);
        Optional<Court> court = courtService.findOne(courtId);

        return court.map(response -> ResponseEntity.ok().body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /courts/:courtId : delete the Court by courtId.
     *
     * @param courtId the courtId of the Court to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/courts/{courtId}")
    public ResponseEntity<Void> deleteCourt(@PathVariable Long courtId) {
        log.debug("REST request to delete Court : {}", courtId);
        courtService.delete(courtId);

        return ResponseEntity.ok().build();
    }
}
