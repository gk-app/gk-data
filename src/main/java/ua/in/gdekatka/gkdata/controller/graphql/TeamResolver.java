package ua.in.gdekatka.gkdata.controller.graphql;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.in.gdekatka.gkdata.model.Player;
import ua.in.gdekatka.gkdata.model.Team;
import ua.in.gdekatka.gkdata.repository.PlayerRepository;

import java.util.List;

@Component
public class TeamResolver implements GraphQLResolver<Team> {

    @Autowired
    private PlayerRepository playerRepository;

    public List<Player> getPlayers(Team team) {
        return playerRepository.findPlayersOfTeam(team.getTeamId());
    }
}
