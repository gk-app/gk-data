package ua.in.gdekatka.gkdata.controller.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.in.gdekatka.gkdata.config.Constants;
import ua.in.gdekatka.gkdata.model.Player;
import ua.in.gdekatka.gkdata.service.PlayerService;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Player.
 */
@RestController
@RequestMapping(Constants.REST_PATH_PREFIX)
public class PlayerController {

    private final Logger log = LoggerFactory.getLogger(PlayerController.class);

    private final PlayerService playerService;

    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    /**
     * POST  /players : Create a new Player.
     *
     * @param player the Player to create
     * @return the ResponseEntity with status 201 (Created) and with body the new Player, or with status 400 (Bad Request) if the Player has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/players")
    public ResponseEntity<Player> createPlayer(@Valid @RequestBody Player player) throws URISyntaxException {
        log.debug("REST request to save Player : {}", player);
        if (player.getPlayerId() != null) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        Player result = playerService.save(player);

        return ResponseEntity.created(new URI(Constants.REST_PATH_PREFIX + "/players/" + result.getPlayerId()))
            .body(result);
    }

    /**
     * PUT  /players : Updates an existing Player.
     *
     * @param player the Player to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated Player,
     * or with status 400 (Bad Request) if the Player is not valid,
     * or with status 500 (Internal Server Error) if the Player couldn't be updated
     */
    @PutMapping("/players/{playerId}")
    public ResponseEntity<Player> updatePlayer(@Valid @RequestBody Player player, @PathVariable Long playerId) {
        log.debug("REST request to update Player : {}", player);
        if (player.getPlayerId() == null || !player.getPlayerId().equals(playerId)) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        Player result = playerService.save(player);

        return ResponseEntity.ok()
            .body(result);
    }

    /**
     * GET  /players : get all the Player.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of Player in body
     */
    @GetMapping("/players")
    public List<Player> getAllPlayers() {
        log.debug("REST request to get all Players");

        return playerService.findAll();
    }

    /**
     * GET  /players/:playerId : get the Player by playerId.
     *
     * @param playerId the playerId of the Player to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the Player, or with status 404 (Not Found)
     */
    @GetMapping("/players/{playerId}")
    public ResponseEntity<Player> getPlayer(@PathVariable Long playerId) {
        log.debug("REST request to get Player : {}", playerId);
        Optional<Player> player = playerService.findOne(playerId);

        return player.map(response -> ResponseEntity.ok().body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /players/:playerId : delete the Player by playerId.
     *
     * @param playerId the playerId of the Player to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/players/{playerId}")
    public ResponseEntity<Void> deletePlayer(@PathVariable Long playerId) {
        log.debug("REST request to delete Player : {}", playerId);
        playerService.delete(playerId);

        return ResponseEntity.ok().build();
    }

}
