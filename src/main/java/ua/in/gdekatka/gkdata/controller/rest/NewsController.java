package ua.in.gdekatka.gkdata.controller.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.in.gdekatka.gkdata.config.Constants;
import ua.in.gdekatka.gkdata.model.News;
import ua.in.gdekatka.gkdata.service.NewsService;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing News.
 */
@RestController
@RequestMapping(Constants.REST_PATH_PREFIX)
public class NewsController {

    private final Logger log = LoggerFactory.getLogger(NewsController.class);

    private final NewsService newsService;

    public NewsController(NewsService newsService) {
        this.newsService = newsService;
    }

    /**
     * POST  /news : Create a new News.
     *
     * @param news the News to create
     * @return the ResponseEntity with status 201 (Created) and with body the new News, or with status 400 (Bad Request) if the News has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/news")
    public ResponseEntity<News> createNews(@Valid @RequestBody News news) throws URISyntaxException {
        log.debug("REST request to save News : {}", news);
        if (news.getNewsId() != null) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        News result = newsService.save(news);

        return ResponseEntity.created(new URI(Constants.REST_PATH_PREFIX + "/news/" + result.getNewsId()))
            .body(result);
    }

    /**
     * PUT  /news : Updates an existing News.
     *
     * @param news the News to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated News,
     * or with status 400 (Bad Request) if the News is not valid,
     * or with status 500 (Internal Server Error) if the News couldn't be updated
     */
    @PutMapping("/news/{newsId}")
    public ResponseEntity<News> updateNews(@Valid @RequestBody News news, @PathVariable Long newsId) {
        log.debug("REST request to update News : {}", news);
        if (news.getNewsId() == null || !news.getNewsId().equals(newsId)) {

            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        News result = newsService.save(news);

        return ResponseEntity.ok()
            .body(result);
    }

    /**
     * GET  /news : get all the News.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of News in body
     */
    @GetMapping("/news")
    public List<News> getAllNewss() {
        log.debug("REST request to get all Newss");

        return newsService.findAll();
    }

    /**
     * GET  /news/:newsId : get the News by newsId.
     *
     * @param newsId the newsId of the News to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the News, or with status 404 (Not Found)
     */
    @GetMapping("/news/{newsId}")
    public ResponseEntity<News> getNews(@PathVariable Long newsId) {
        log.debug("REST request to get News : {}", newsId);
        Optional<News> news = newsService.findOne(newsId);

        return news.map(response -> ResponseEntity.ok().body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /news/:newsId : delete the News by newsId.
     *
     * @param newsId the newsId of the News to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/news/{newsId}")
    public ResponseEntity<Void> deleteNews(@PathVariable Long newsId) {
        log.debug("REST request to delete News : {}", newsId);
        newsService.delete(newsId);

        return ResponseEntity.ok().build();
    }
}
