package ua.in.gdekatka.gkdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.in.gdekatka.gkdata.model.Award;

import java.util.List;

@SuppressWarnings("unused")
@Repository
public interface AwardRepository extends JpaRepository<Award, Long> {

    @Query("SELECT a FROM Award a JOIN a.players ap WHERE ap.playerId = :playerId")
    List<Award> findAwardsOfPlayer(@Param("playerId") Long playerId);
}
