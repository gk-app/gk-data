package ua.in.gdekatka.gkdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.in.gdekatka.gkdata.model.Rules;

@SuppressWarnings("unused")
@Repository
public interface RulesRepository extends JpaRepository<Rules, Long> {
}
