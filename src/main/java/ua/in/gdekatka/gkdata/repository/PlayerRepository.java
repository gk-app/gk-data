package ua.in.gdekatka.gkdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.in.gdekatka.gkdata.model.Player;

import java.util.List;

@SuppressWarnings("unused")
@Repository
public interface PlayerRepository extends JpaRepository<Player, Long> {

    @Query("SELECT p FROM Player p WHERE p.team.teamId = :teamId")
    List<Player> findPlayersOfTeam(@Param("teamId") Long teamId);
}
