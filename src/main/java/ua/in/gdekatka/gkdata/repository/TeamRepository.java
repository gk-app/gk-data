package ua.in.gdekatka.gkdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.in.gdekatka.gkdata.model.Team;

@SuppressWarnings("unused")
@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {

    @Query("SELECT t FROM Team t WHERE t.teamId = (SELECT p.team FROM Player p WHERE p.playerId = :playerId )")
    Team findTeamOfPlayer(@Param("playerId") Long playerId);
}
