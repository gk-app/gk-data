package ua.in.gdekatka.gkdata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ua.in.gdekatka.gkdata.model.Court;

@SuppressWarnings("unused")
@Repository
public interface CourtRepository extends JpaRepository<Court, Long> {

    @Query("SELECT c FROM Court c WHERE c.courtId = (SELECT p.court FROM Player p WHERE p.playerId = :playerId )")
    Court findCourtOfPlayer(@Param("playerId") Long playerId);
}
