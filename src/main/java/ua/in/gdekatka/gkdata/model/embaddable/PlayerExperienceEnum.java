package ua.in.gdekatka.gkdata.model.embaddable;

public enum PlayerExperienceEnum {
    UNCALCULATED,
    ROOKIE,
    SOPHOMORE,
    SENIOR
}
