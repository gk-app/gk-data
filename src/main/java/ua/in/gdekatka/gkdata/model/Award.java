package ua.in.gdekatka.gkdata.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import ua.in.gdekatka.gkdata.model.embaddable.AwardCategoryEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "award")
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "awardId", scope = Award.class)
@Getter @Setter @Builder
@NoArgsConstructor @AllArgsConstructor
public class Award implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator", sequenceName = "award_award_id_seq", allocationSize = 10)
    private Long awardId;

    @NotBlank
    @Length(max = 50)
    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @Length(max = 255)
    @Column(name = "avatar")
    private String avatar;

    @Length(max = 255)
    @Column(name = "thumbnail")
    private String thumbnail;

    @NotNull
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "award_category", nullable = false)
    private AwardCategoryEnum awardCategory;

    @Length(max = 255)
    @Column(name = "description")
    private String description;

    @ManyToMany(mappedBy = "awards")
    private List<Player> players;

    @Override
    public String toString() {
        return "Award{" +
            "awardId=" + awardId +
            ", name='" + name + '\'' +
            ", avatar='" + avatar + '\'' +
            ", thumbnail='" + thumbnail + '\'' +
            ", awardCategory=" + awardCategory +
            ", description='" + description + '\'' +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Award)) return false;
        Award award = (Award) o;
        return awardId != null && Objects.equals(getAwardId(), award.getAwardId());
    }

    /**
     * hashCode always returns the same value to make possible HashSet and the like collection usage.
     */
    @Override
    public int hashCode() {
        return 106;
    }
}



