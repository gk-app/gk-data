package ua.in.gdekatka.gkdata.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "court_image")
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "courtImageId", scope = CourtImage.class)
@Getter @Setter @Builder
@NoArgsConstructor @AllArgsConstructor
public class CourtImage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator", sequenceName = "court_image_court_image_id_seq", allocationSize = 10)
    @Column(name = "court_image_id")
    private Long courtImageId;

    @NotBlank
    @Length(max = 255)
    @Column(name = "link", nullable = false)
    private String link;

    @Length(max = 255)
    @Column(name = "thumbnail")
    private String thumbnail;

    @Override
    public String toString() {
        return "CourtImage{" +
            "courtImageId=" + courtImageId +
            ", link='" + link + '\'' +
            ", thumbnail='" + thumbnail + '\'' +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CourtImage)) return false;
        CourtImage image = (CourtImage) o;
        return courtImageId != null && Objects.equals(getCourtImageId(), image.getCourtImageId());
    }

    /**
     * hashCode always returns the same value to make possible HashSet and the like collection usage.
     */
    @Override
    public int hashCode() {
        return 105;
    }
}
