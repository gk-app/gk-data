package ua.in.gdekatka.gkdata.model.embaddable;

public enum AwardCategoryEnum {
    TEAM,
    PERSONAL,
    COMMUNITY
}
