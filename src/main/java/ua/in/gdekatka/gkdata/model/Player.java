package ua.in.gdekatka.gkdata.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.validator.constraints.Length;
import ua.in.gdekatka.gkdata.model.embaddable.PlayerExperienceEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "player")
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "playerId", scope = Player.class)
@JsonIgnoreProperties(value = {"lastSeen"}, allowGetters = true)
@Getter @Setter @Builder
@NoArgsConstructor @AllArgsConstructor
public class Player implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator", sequenceName = "player_player_id_seq", allocationSize = 10)
    @Column(name = "player_id")
    private Long playerId;

    @NotBlank
    @Length(max = 20)
    @Column(name = "nickname", length = 20, nullable = false, unique = true)
    private String nickname;

    @NotBlank
    @Email
    @Length(max = 50)
    @Column(name = "email", length = 50, nullable = false, unique = true)
    private String email;

    @Length(max = 255)
    @Column(name = "avatar")
    private String avatar;

    @Length(max = 255)
    @Column(name = "thumbnail")
    private String thumbnail;

    @NotBlank
    @Length(max = 20)
    @Column(name = "fname", length = 20, nullable = false)
    private String fname;

    @NotBlank
    @Length(max = 20)
    @Column(name = "lname", length = 20, nullable = false)
    private String lname;

    @NotNull
    @Column(name = "is_male", nullable = false)
    private Boolean isMale;

    @NotNull
    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

//TODO implement some filter on a highest level that will update only this property
    @Column(name = "last_seen")
    private LocalDateTime lastSeen = LocalDateTime.now();

    @CreationTimestamp
    @Column(name = "created_at", updatable = false, insertable = false)
    private LocalDate createdAt;

    /**
     * Field is not persisted in DB and is calculated dynamically during GET request.
     */
    @Transient
    private PlayerExperienceEnum experience;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "court")
    private Court court;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "team")
    private Team team;

    @ManyToMany
    @JoinTable(name = "player_awards",
        joinColumns = @JoinColumn(name = "player_id"),
        inverseJoinColumns = @JoinColumn(name = "award_id"))
    private List<Award> awards;

    @SuppressWarnings("unused")
    @JsonGetter("experience")
    public PlayerExperienceEnum calculateExperience() {

        if (this.createdAt != null) {

            long regDuration = ChronoUnit.DAYS.between(this.createdAt, LocalDate.now());

            if (regDuration <= 2) {
                return PlayerExperienceEnum.ROOKIE;
            } else if (regDuration <= 4) {
                return PlayerExperienceEnum.SOPHOMORE;
            } else {
                return PlayerExperienceEnum.SENIOR;
            }
        } else {
            return PlayerExperienceEnum.UNCALCULATED;
        }
    }

    @Override
    public String toString() {
        return "Player{" +
            "playerId=" + playerId +
            ", nickname='" + nickname + '\'' +
            ", email='" + email + '\'' +
            ", avatar='" + avatar + '\'' +
            ", thumbnail='" + thumbnail + '\'' +
            ", fname='" + fname + '\'' +
            ", lname='" + lname + '\'' +
            ", isMale='" + isMale + '\'' +
            ", dateOfBirth='" + dateOfBirth + '\'' +
            ", lastSeen=" + lastSeen +
            ", createdAt=" + createdAt +
            ", experience=" + experience +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Player)) return false;
        Player player = (Player) o;
        return playerId != null && Objects.equals(getPlayerId(), player.getPlayerId());
    }

    /**
     * hashCode always returns the same value to make possible HashSet and the like collection usage.
     */
    @Override
    public int hashCode() {
        return 101;
    }
}
