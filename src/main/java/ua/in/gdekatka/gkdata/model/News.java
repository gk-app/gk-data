package ua.in.gdekatka.gkdata.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "news")
@JsonIgnoreProperties(value = {"createdAt"}, allowGetters = true)
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "newsId", scope = News.class)
@Getter @Setter @Builder
@NoArgsConstructor @AllArgsConstructor
public class News implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator", sequenceName = "news_news_id_seq", allocationSize = 10)
    private Long newsId;

    @NotBlank
    @Length(max = 50)
    @Column(name = "title", length = 50, nullable = false)
    private String title;

    @NotBlank
    @Length(max = 500)
    @Column(name = "content")
    private String content;

    @Length(max = 255)
    @Column(name = "avatar")
    private String avatar;

    @Length(max = 255)
    @Column(name = "thumbnail")
    private String thumbnail;

    /**
     * Field is set by DB once during creation of the entity and is always null on PUT and POST request.
     * */
    @Column(name = "created_at", updatable = false, insertable = false)
    private LocalDate createdAt;

    @Override
    public String toString() {
        return "News{" +
            "newsId=" + newsId +
            ", title='" + title + '\'' +
            ", content='" + content + '\'' +
            ", avatar='" + avatar + '\'' +
            ", thumbnail='" + thumbnail + '\'' +
            ", createdAt=" + createdAt +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof News)) return false;
        News news = (News) o;
        return newsId != null && Objects.equals(getNewsId(), news.getNewsId());
    }

    /**
     * hashCode always returns the same value to make possible HashSet and the like collection usage.
     */
    @Override
    public int hashCode() {
        return 83;
    }
}
