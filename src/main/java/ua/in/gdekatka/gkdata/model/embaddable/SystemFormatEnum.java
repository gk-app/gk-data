package ua.in.gdekatka.gkdata.model.embaddable;

public enum SystemFormatEnum {
    KNOCKOUT,
    GROUP,
    MIXED
}
