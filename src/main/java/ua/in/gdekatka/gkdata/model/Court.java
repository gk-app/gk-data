package ua.in.gdekatka.gkdata.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "court")
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "courtId", scope = Court.class)
@JsonIgnoreProperties(value = {"createdAt"}, allowGetters = true)
@Getter @Setter @Builder
@NoArgsConstructor @AllArgsConstructor
public class Court implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator", sequenceName = "court_court_id_seq", allocationSize = 10)
    @Column(name = "court_id")
    private Long courtId;

    @NotBlank
    @Length(max = 50)
    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @Length(max = 255)
    @Column(name = "avatar")
    private String avatar;

    @Length(max = 255)
    @Column(name = "thumbnail")
    private String thumbnail;

    @NotNull
    @Column(name = "latitude", nullable = false)
    private Double latitude;

    @NotNull
    @Column(name = "longitude", nullable = false)
    private Double longitude;

    @NotNull
    @Min(1)
    @Max(10)
    @Column(name = "rating")
    private Integer rating = 5;

    @Length(max = 300)
    @Column(name = "description", length = 300)
    private String description;

    /**
     * Field is set by DB once during creation of the entity and is always null on PUT and POST request.
     */
    @Column(name = "created_at", updatable = false, insertable = false)
    private LocalDate createdAt;

    @OneToMany(mappedBy = "court")
    private List<Player> players;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "court_id")
    private List<CourtImage> images = new ArrayList<>();

    @Override
    public String toString() {
        return "Court{" +
            "courtId=" + courtId +
            ", name='" + name + '\'' +
            ", avatar='" + avatar + '\'' +
            ", thumbnail='" + thumbnail + '\'' +
            ", latitude=" + latitude +
            ", longitude=" + longitude +
            ", rating=" + rating +
            ", description='" + description + '\'' +
            ", createdAt=" + createdAt +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Court)) return false;
        Court court = (Court) o;
        return courtId != null && Objects.equals(getCourtId(), court.getCourtId());
    }

    /**
     * hashCode always returns the same value to make possible HashSet and the like collection usage.
     */
    @Override
    public int hashCode() {
        return 102;
    }
}



