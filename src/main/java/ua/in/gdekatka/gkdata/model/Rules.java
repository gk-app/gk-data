package ua.in.gdekatka.gkdata.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import ua.in.gdekatka.gkdata.model.embaddable.DrawEnum;
import ua.in.gdekatka.gkdata.model.embaddable.SystemFormatEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

//TODO set default values

/**
 * Rules.
 */
@Entity
@Table(name = "rules")
@JsonIgnoreProperties(value = {"createdAt"}, allowGetters = true)
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "rulesId", scope = Rules.class)
@Getter @Setter @Builder
@NoArgsConstructor @AllArgsConstructor
public class Rules implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator", sequenceName = "rules_rules_id_seq", allocationSize = 10)
    private Long rulesId;

//TODO in case of unique constraint violation 500 is thrown

    @NotBlank
    @Length(max = 50)
    @Column(name = "name", length = 50, unique = true, nullable = false)
    private String name;

    @NotNull
    @Max(5)
    @Column(name = "team_format", length = 3, nullable = false)
    private Integer teamFormat = 5;

    @NotNull
    @Length(max = 4)
    @Column(name = "time_format", length = 4, nullable = false)
    private String timeFormat;

    @NotNull
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "system_format_id", nullable = false)
    private SystemFormatEnum systemFormat;

    @NotNull
    @Column(name = "attack_time", nullable = false)
    private Integer attackTime;

    @NotNull
    @Column(name = "teams_limit", nullable = false)
    private Integer teamsLimit = 0;

    @NotNull
    @Enumerated(EnumType.ORDINAL)
    @Column(name = "draw_id", nullable = false)
    private DrawEnum drawInfo;

    @Length(max = 300)
    @Column(name = "description", length = 300)
    private String description;

    /**
     * Field is set by DB once during creation of the entity and is always null on PUT and POST request.
     * */
    @Column(name = "created_at", updatable = false, insertable = false)
    private LocalDate createdAt;

    @Override
    public String toString() {
        return "Rules{" +
            "rulesId=" + rulesId +
            ", name='" + name + '\'' +
            ", teamFormat=" + teamFormat +
            ", timeFormat='" + timeFormat + '\'' +
            ", systemFormat=" + systemFormat +
            ", attackTime=" + attackTime +
            ", teamsLimit=" + teamsLimit +
            ", drawInfo=" + drawInfo +
            ", description='" + description + '\'' +
            ", createdAt=" + createdAt +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rules)) return false;
        Rules rules = (Rules) o;
        return rulesId != null && Objects.equals(getRulesId(), rules.getRulesId());
    }

    /**
     * hashCode always returns the same value to make possible HashSet and the like collection usage.
     */
    @Override
    public int hashCode() {
        return 104;
    }
}
