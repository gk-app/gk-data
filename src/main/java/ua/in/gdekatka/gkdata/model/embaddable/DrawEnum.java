package ua.in.gdekatka.gkdata.model.embaddable;

public enum DrawEnum {
    MANUAL,
    AUTOMATED,
    MIXED
}
