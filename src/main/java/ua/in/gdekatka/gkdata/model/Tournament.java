package ua.in.gdekatka.gkdata.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "tournament")
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "tournamentId", scope = Tournament.class)
@JsonIgnoreProperties(value = {"createdAt"}, allowGetters = true)
@Getter @Setter @Builder
@NoArgsConstructor @AllArgsConstructor
public class Tournament implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator", sequenceName = "tournament_tournament_id_seq", allocationSize = 10)
    @Column(name = "tournament_id")
    private Long tournamentId;

    @NotBlank
    @Length(max = 20)
    @Column(name = "name", length = 20, nullable = false)
    private String name;

    @Length(max = 255)
    @Column(name = "avatar")
    private String avatar;

    @Length(max = 255)
    @Column(name = "thumbnail")
    private String thumbnail;

    @NotNull
    @FutureOrPresent
    @Column(name = "date", nullable = false)
    private LocalDateTime date;

    /**
     * Field is set by DB once during creation of the entity and is always null on PUT and POST request.
     */
    @Column(name = "created_at", updatable = false, insertable = false)
    private LocalDate createdAt;

    @OneToOne
    @JoinColumn(name = "organizer", nullable = false)
    private Player organizer;

    @OneToOne
    @JoinColumn(name = "place", nullable = false)
    private Court place;

    @OneToOne
    @JoinColumn(name = "rules", nullable = false)
    private Rules rules;

    @OneToOne
    @JoinColumn(name = "coorganizer")
    private Player coorganizer;

    @OneToOne
    @JoinColumn(name = "judge_one")
    private Player judgeOne;

    @OneToOne
    @JoinColumn(name = "judge_two")
    private Player judgeTwo;

    @Override
    public String toString() {
        return "Tournament{" +
            "tournamentId=" + tournamentId +
            ", name='" + name + '\'' +
            ", avatar='" + avatar + '\'' +
            ", thumbnail='" + thumbnail + '\'' +
            ", date=" + date +
            ", createdAt=" + createdAt +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tournament)) return false;
        Tournament tournament = (Tournament) o;
        return tournamentId != null && Objects.equals(getTournamentId(), tournament.getTournamentId());
    }

    /**
     * hashCode always returns the same value to make possible HashSet and the like collection usage.
     */
    @Override
    public int hashCode() {
        return 107;
    }
}
