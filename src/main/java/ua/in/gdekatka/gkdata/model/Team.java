package ua.in.gdekatka.gkdata.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "team")
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "teamId", scope = Team.class)
@JsonIgnoreProperties(value = {"createdAt", "lastSeen"}, allowGetters = true)
@Getter @Setter @Builder
@NoArgsConstructor @AllArgsConstructor
public class Team implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator", sequenceName = "team_team_id_seq", allocationSize = 10)
    private Long teamId;

    @NotBlank
    @Length(max = 20)
    @Column(name = "name", length = 20, nullable = false, unique = true)
    private String name;

    @Length(max = 255)
    @Column(name = "avatar")
    private String avatar;

    @Length(max = 255)
    @Column(name = "thumbnail")
    private String thumbnail;

    /**
     * Field is set by DB once during creation of the entity and is always null on PUT and POST request.
     */
    @Column(name = "created_at", updatable = false, insertable = false)
    private LocalDate createdAt;

    @OneToOne
    @JoinColumn(name = "captain_id")
    private Player captain;

    @OneToMany(mappedBy = "team", cascade = {CascadeType.MERGE})
    private List<Player> players;

    @Override
    public String toString() {
        return "Team{" +
            "teamId=" + teamId +
            ", name='" + name + '\'' +
            ", avatar='" + avatar + '\'' +
            ", thumbnail='" + thumbnail + '\'' +
            ", createdAt=" + createdAt +
            '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Team)) return false;
        Team team = (Team) o;
        return teamId != null && Objects.equals(getTeamId(), team.getTeamId());
    }

    /**
     * hashCode always returns the same value to make possible HashSet and the like collection usage.
     */
    @Override
    public int hashCode() {
        return 103;
    }
}
