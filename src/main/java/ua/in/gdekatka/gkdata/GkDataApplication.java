package ua.in.gdekatka.gkdata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GkDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(GkDataApplication.class, args);
    }

}
