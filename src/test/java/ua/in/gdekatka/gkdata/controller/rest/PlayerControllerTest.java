package ua.in.gdekatka.gkdata.controller.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ua.in.gdekatka.gkdata.config.Constants;
import ua.in.gdekatka.gkdata.controller.rest.PlayerController;
import ua.in.gdekatka.gkdata.model.Player;
import ua.in.gdekatka.gkdata.service.PlayerService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ua.in.gdekatka.gkdata.util.EntityCreator.createBasicPlayer;

@RunWith(SpringRunner.class)
@WebMvcTest(PlayerController.class)
public class PlayerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private PlayerService playerService;

    private static Player persistedPlayer;
    private static String notPersistedPlayerJson;
    private static String persistedPlayerJson;

    @Before
    public void setUp() throws Exception {
        Player notPersistedPlayer = createBasicPlayer();
        persistedPlayer = createBasicPlayer();
        persistedPlayer.setPlayerId(1L);

        notPersistedPlayerJson = objectMapper.writeValueAsString(notPersistedPlayer);
        persistedPlayerJson = objectMapper.writeValueAsString(persistedPlayer);
    }

    @Test
    public void post_validPlayer_OK() throws Exception {
        when(playerService.save(any(Player.class))).thenReturn(persistedPlayer);

        mockMvc.perform(post(Constants.REST_PATH_PREFIX + "/players")
                .accept(MediaType.APPLICATION_JSON)
                .content(notPersistedPlayerJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(persistedPlayerJson));
    }

    @Test
    public void post_playerWithId_BAD_REQUEST() throws Exception {
        mockMvc.perform(
                post(Constants.REST_PATH_PREFIX + "/players")
                        .accept(MediaType.APPLICATION_JSON)
                        .content(persistedPlayerJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void put_validPlayer_OK() throws Exception {
        when(playerService.save(any(Player.class))).thenReturn(persistedPlayer);

        mockMvc.perform(put(Constants.REST_PATH_PREFIX + "/players/" + persistedPlayer.getPlayerId())
                .accept(MediaType.APPLICATION_JSON)
                .content(persistedPlayerJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(persistedPlayerJson));
    }

    @Test
    public void put_playerWithoutId_BAD_REQUEST() throws Exception {
        mockMvc.perform(
                put(Constants.REST_PATH_PREFIX + "/players/" + persistedPlayer.getPlayerId())
                        .accept(MediaType.APPLICATION_JSON)
                        .content(notPersistedPlayerJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void get_allPlayers_OK() throws Exception {
        List<Player> players = Arrays.asList(createBasicPlayer(), createBasicPlayer(), createBasicPlayer());
        String playersJson = objectMapper.writeValueAsString(players);
        when(playerService.findAll()).thenReturn(players);

        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/players"))
                .andExpect(status().isOk())
                .andExpect(content().json(playersJson));
    }

    @Test
    public void get_existedPlayer_OK() throws Exception {
        when(playerService.findOne(persistedPlayer.getPlayerId())).thenReturn(Optional.of(persistedPlayer));

        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/players/" + persistedPlayer.getPlayerId()))
                .andExpect(status().isOk())
                .andExpect(content().json(persistedPlayerJson));
    }

    @Test
    public void get_notExistedPlayer_NOT_FOUND() throws Exception {
        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/players/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void delete_player_OK() throws Exception {
        mockMvc.perform(delete(Constants.REST_PATH_PREFIX + "/players/1"))
                .andExpect(status().isOk());
    }
}
