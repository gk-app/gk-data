package ua.in.gdekatka.gkdata.controller.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ua.in.gdekatka.gkdata.config.Constants;
import ua.in.gdekatka.gkdata.controller.rest.CourtController;
import ua.in.gdekatka.gkdata.model.Court;
import ua.in.gdekatka.gkdata.service.CourtService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ua.in.gdekatka.gkdata.util.EntityCreator.createBasicCourt;

@RunWith(SpringRunner.class)
@WebMvcTest(CourtController.class)
public class CourtControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private CourtService courtService;

    private static Court persistedCourt;
    private static String notPersistedCourtJson;
    private static String persistedCourtJson;

    @Before
    public void setUp() throws Exception {
        Court notPersistedCourt = createBasicCourt();
        persistedCourt = createBasicCourt();
        persistedCourt.setCourtId(1L);

        notPersistedCourtJson = objectMapper.writeValueAsString(notPersistedCourt);
        persistedCourtJson = objectMapper.writeValueAsString(persistedCourt);
    }

    @Test
    public void post_validCourt_OK() throws Exception {
        when(courtService.save(any(Court.class))).thenReturn(persistedCourt);

        mockMvc.perform(post(Constants.REST_PATH_PREFIX + "/courts")
                .accept(MediaType.APPLICATION_JSON)
                .content(notPersistedCourtJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(persistedCourtJson));
    }

    @Test
    public void post_courtWithId_BAD_REQUEST() throws Exception {
        mockMvc.perform(
                post(Constants.REST_PATH_PREFIX + "/courts")
                        .accept(MediaType.APPLICATION_JSON)
                        .content(persistedCourtJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void put_validCourt_OK() throws Exception {
        when(courtService.save(any(Court.class))).thenReturn(persistedCourt);

        mockMvc.perform(put(Constants.REST_PATH_PREFIX + "/courts/" + persistedCourt.getCourtId())
                .accept(MediaType.APPLICATION_JSON)
                .content(persistedCourtJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(persistedCourtJson));
    }

    @Test
    public void put_courtWithoutId_BAD_REQUEST() throws Exception {
        mockMvc.perform(
                put(Constants.REST_PATH_PREFIX + "/courts/" + persistedCourt.getCourtId())
                        .accept(MediaType.APPLICATION_JSON)
                        .content(notPersistedCourtJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void get_allCourts_OK() throws Exception {
        List<Court> courts = Arrays.asList(createBasicCourt(), createBasicCourt(), createBasicCourt());
        String courtsJson = objectMapper.writeValueAsString(courts);
        when(courtService.findAll()).thenReturn(courts);

        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/courts"))
                .andExpect(status().isOk())
                .andExpect(content().json(courtsJson));
    }

    @Test
    public void get_existedCourt_OK() throws Exception {
        when(courtService.findOne(persistedCourt.getCourtId())).thenReturn(Optional.of(persistedCourt));

        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/courts/" + persistedCourt.getCourtId()))
                .andExpect(status().isOk())
                .andExpect(content().json(persistedCourtJson));
    }

    @Test
    public void get_notExistedCourt_NOT_FOUND() throws Exception {
        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/courts/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void delete_court_OK() throws Exception {
        mockMvc.perform(delete(Constants.REST_PATH_PREFIX + "/courts/1"))
                .andExpect(status().isOk());
    }
}
