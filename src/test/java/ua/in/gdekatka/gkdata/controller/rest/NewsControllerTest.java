package ua.in.gdekatka.gkdata.controller.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ua.in.gdekatka.gkdata.config.Constants;
import ua.in.gdekatka.gkdata.controller.rest.NewsController;
import ua.in.gdekatka.gkdata.model.News;
import ua.in.gdekatka.gkdata.service.NewsService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ua.in.gdekatka.gkdata.util.EntityCreator.createNews;

@RunWith(SpringRunner.class)
@WebMvcTest(NewsController.class)
public class NewsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private NewsService newsService;

    private static News persistedNews;
    private static String notPersistedNewsJson;
    private static String persistedNewsJson;

    @Before
    public void setUp() throws Exception {
        News notPersistedNews = createNews();
        persistedNews = createNews();
        persistedNews.setNewsId(1L);

        notPersistedNewsJson = objectMapper.writeValueAsString(notPersistedNews);
        persistedNewsJson = objectMapper.writeValueAsString(persistedNews);
    }

    @Test
    public void post_validNews_OK() throws Exception {
        when(newsService.save(any(News.class))).thenReturn(persistedNews);

        mockMvc.perform(post(Constants.REST_PATH_PREFIX + "/news")
                .accept(MediaType.APPLICATION_JSON)
                .content(notPersistedNewsJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(persistedNewsJson));
    }

    @Test
    public void post_newsWithId_BAD_REQUEST() throws Exception {
        mockMvc.perform(
                post(Constants.REST_PATH_PREFIX + "/news")
                        .accept(MediaType.APPLICATION_JSON)
                        .content(persistedNewsJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void put_validNews_OK() throws Exception {
        when(newsService.save(any(News.class))).thenReturn(persistedNews);

        mockMvc.perform(put(Constants.REST_PATH_PREFIX + "/news/" + persistedNews.getNewsId())
                .accept(MediaType.APPLICATION_JSON)
                .content(persistedNewsJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(persistedNewsJson));
    }

    @Test
    public void put_newsWithoutId_BAD_REQUEST() throws Exception {
        mockMvc.perform(
                put(Constants.REST_PATH_PREFIX + "/news/" + persistedNews.getNewsId())
                        .accept(MediaType.APPLICATION_JSON)
                        .content(notPersistedNewsJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void get_allNews_OK() throws Exception {
        List<News> news = Arrays.asList(createNews(), createNews(), createNews());
        String newsJson = objectMapper.writeValueAsString(news);
        when(newsService.findAll()).thenReturn(news);

        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/news"))
                .andExpect(status().isOk())
                .andExpect(content().json(newsJson));
    }

    @Test
    public void get_existedNews_OK() throws Exception {
        when(newsService.findOne(persistedNews.getNewsId())).thenReturn(Optional.of(persistedNews));

        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/news/" + persistedNews.getNewsId()))
                .andExpect(status().isOk())
                .andExpect(content().json(persistedNewsJson));
    }

    @Test
    public void get_notExistedNews_NOT_FOUND() throws Exception {
        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/news/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void delete_news_OK() throws Exception {
        mockMvc.perform(delete(Constants.REST_PATH_PREFIX + "/news/1"))
                .andExpect(status().isOk());
    }
}
