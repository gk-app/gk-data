package ua.in.gdekatka.gkdata.controller.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ua.in.gdekatka.gkdata.config.Constants;
import ua.in.gdekatka.gkdata.controller.rest.AwardController;
import ua.in.gdekatka.gkdata.model.Award;
import ua.in.gdekatka.gkdata.service.AwardService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ua.in.gdekatka.gkdata.util.EntityCreator.createAward;

@RunWith(SpringRunner.class)
@WebMvcTest(AwardController.class)
public class AwardControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private AwardService awardService;

    private static Award persistedAward;
    private static String notPersistedAwardJson;
    private static String persistedAwardJson;

    @Before
    public void setUp() throws Exception {
        Award notPersistedAward = createAward();
        persistedAward = createAward();
        persistedAward.setAwardId(1L);

        notPersistedAwardJson = objectMapper.writeValueAsString(notPersistedAward);
        persistedAwardJson = objectMapper.writeValueAsString(persistedAward);
    }

    @Test
    public void post_validAward_OK() throws Exception {
        when(awardService.save(any(Award.class))).thenReturn(persistedAward);

        mockMvc.perform(post(Constants.REST_PATH_PREFIX + "/awards")
                .accept(MediaType.APPLICATION_JSON)
                .content(notPersistedAwardJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(persistedAwardJson));
    }

    @Test
    public void post_awardWithId_BAD_REQUEST() throws Exception {
        mockMvc.perform(
                post(Constants.REST_PATH_PREFIX + "/awards")
                        .accept(MediaType.APPLICATION_JSON)
                        .content(persistedAwardJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void put_validAward_OK() throws Exception {
        when(awardService.save(any(Award.class))).thenReturn(persistedAward);

        mockMvc.perform(put(Constants.REST_PATH_PREFIX + "/awards/" + persistedAward.getAwardId())
                .accept(MediaType.APPLICATION_JSON)
                .content(persistedAwardJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(persistedAwardJson));
    }

    @Test
    public void put_awardWithoutId_BAD_REQUEST() throws Exception {
        mockMvc.perform(
                put(Constants.REST_PATH_PREFIX + "/awards/" + persistedAward.getAwardId())
                        .accept(MediaType.APPLICATION_JSON)
                        .content(notPersistedAwardJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void get_allAwards_OK() throws Exception {
        List<Award> awards = Arrays.asList(createAward(), createAward(), createAward());
        String awardsJson = objectMapper.writeValueAsString(awards);
        when(awardService.findAll()).thenReturn(awards);

        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/awards"))
                .andExpect(status().isOk())
                .andExpect(content().json(awardsJson));
    }

    @Test
    public void get_existedAward_OK() throws Exception {
        when(awardService.findOne(persistedAward.getAwardId())).thenReturn(Optional.of(persistedAward));

        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/awards/" + persistedAward.getAwardId()))
                .andExpect(status().isOk())
                .andExpect(content().json(persistedAwardJson));
    }

    @Test
    public void get_notExistedAward_NOT_FOUND() throws Exception {
        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/awards/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void delete_award_OK() throws Exception {
        mockMvc.perform(delete(Constants.REST_PATH_PREFIX + "/awards/1"))
                .andExpect(status().isOk());
    }
}
