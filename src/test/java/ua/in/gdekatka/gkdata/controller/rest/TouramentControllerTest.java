package ua.in.gdekatka.gkdata.controller.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ua.in.gdekatka.gkdata.config.Constants;
import ua.in.gdekatka.gkdata.controller.rest.TournamentController;
import ua.in.gdekatka.gkdata.model.Tournament;
import ua.in.gdekatka.gkdata.service.TournamentService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ua.in.gdekatka.gkdata.util.EntityCreator.createBasicTournament;

@RunWith(SpringRunner.class)
@WebMvcTest(TournamentController.class)
public class TouramentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TournamentService tournamentService;

    private static Tournament persistedTournament;
    private static String notPersistedTournamentJson;
    private static String persistedTournamentJson;

    @Before
    public void setUp() throws Exception {
        Tournament notPersistedTournament = createBasicTournament();
        persistedTournament = createBasicTournament();
        persistedTournament.setTournamentId(1L);

        notPersistedTournamentJson = objectMapper.writeValueAsString(notPersistedTournament);
        persistedTournamentJson = objectMapper.writeValueAsString(persistedTournament);
    }

    @Test
    public void post_validTournament_OK() throws Exception {
        when(tournamentService.save(any(Tournament.class))).thenReturn(persistedTournament);

        mockMvc.perform(post(Constants.REST_PATH_PREFIX + "/tournaments")
                .accept(MediaType.APPLICATION_JSON)
                .content(notPersistedTournamentJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(persistedTournamentJson));
    }

    @Test
    public void post_tournamentWithId_BAD_REQUEST() throws Exception {
        mockMvc.perform(
                post(Constants.REST_PATH_PREFIX + "/tournaments")
                        .accept(MediaType.APPLICATION_JSON)
                        .content(persistedTournamentJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void put_validTournament_OK() throws Exception {
        when(tournamentService.save(any(Tournament.class))).thenReturn(persistedTournament);

        mockMvc.perform(put(Constants.REST_PATH_PREFIX + "/tournaments/" + persistedTournament.getTournamentId())
                .accept(MediaType.APPLICATION_JSON)
                .content(persistedTournamentJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(persistedTournamentJson));
    }

    @Test
    public void put_tournamentWithoutId_BAD_REQUEST() throws Exception {
        mockMvc.perform(
                put(Constants.REST_PATH_PREFIX + "/tournaments/" + persistedTournament.getTournamentId())
                        .accept(MediaType.APPLICATION_JSON)
                        .content(notPersistedTournamentJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void get_allTournaments_OK() throws Exception {
        List<Tournament> tournaments = Arrays.asList(createBasicTournament(), createBasicTournament(), createBasicTournament());
        String tournamentsJson = objectMapper.writeValueAsString(tournaments);
        when(tournamentService.findAll()).thenReturn(tournaments);

        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/tournaments"))
                .andExpect(status().isOk())
                .andExpect(content().json(tournamentsJson));
    }

    @Test
    public void get_existedTournament_OK() throws Exception {
        when(tournamentService.findOne(persistedTournament.getTournamentId())).thenReturn(Optional.of(persistedTournament));

        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/tournaments/" + persistedTournament.getTournamentId()))
                .andExpect(status().isOk())
                .andExpect(content().json(persistedTournamentJson));
    }

    @Test
    public void get_notExistedTournament_NOT_FOUND() throws Exception {
        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/tournaments/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void delete_tournament_OK() throws Exception {
        mockMvc.perform(delete(Constants.REST_PATH_PREFIX + "/tournaments/1"))
                .andExpect(status().isOk());
    }
}
