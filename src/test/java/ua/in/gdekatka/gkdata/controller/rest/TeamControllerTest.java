package ua.in.gdekatka.gkdata.controller.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ua.in.gdekatka.gkdata.config.Constants;
import ua.in.gdekatka.gkdata.controller.rest.TeamController;
import ua.in.gdekatka.gkdata.model.Team;
import ua.in.gdekatka.gkdata.service.TeamService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ua.in.gdekatka.gkdata.util.EntityCreator.createBasicTeam;

@RunWith(SpringRunner.class)
@WebMvcTest(TeamController.class)
public class TeamControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TeamService teamService;

    private static Team persistedTeam;
    private static String notPersistedTeamJson;
    private static String persistedTeamJson;

    @Before
    public void setUp() throws Exception {
        Team notPersistedTeam = createBasicTeam();
        persistedTeam = createBasicTeam();
        persistedTeam.setTeamId(1L);

        notPersistedTeamJson = objectMapper.writeValueAsString(notPersistedTeam);
        persistedTeamJson = objectMapper.writeValueAsString(persistedTeam);
    }

    @Test
    public void post_validTeam_OK() throws Exception {
        when(teamService.save(any(Team.class))).thenReturn(persistedTeam);

        mockMvc.perform(post(Constants.REST_PATH_PREFIX + "/teams")
                .accept(MediaType.APPLICATION_JSON)
                .content(notPersistedTeamJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(persistedTeamJson));
    }

    @Test
    public void post_teamWithId_BAD_REQUEST() throws Exception {
        mockMvc.perform(
                post(Constants.REST_PATH_PREFIX + "/teams")
                        .accept(MediaType.APPLICATION_JSON)
                        .content(persistedTeamJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void put_validTeam_OK() throws Exception {
        when(teamService.save(any(Team.class))).thenReturn(persistedTeam);

        mockMvc.perform(put(Constants.REST_PATH_PREFIX + "/teams/" + persistedTeam.getTeamId())
                .accept(MediaType.APPLICATION_JSON)
                .content(persistedTeamJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(persistedTeamJson));
    }

    @Test
    public void put_teamWithoutId_BAD_REQUEST() throws Exception {
        mockMvc.perform(
                put(Constants.REST_PATH_PREFIX + "/teams/" + persistedTeam.getTeamId())
                        .accept(MediaType.APPLICATION_JSON)
                        .content(notPersistedTeamJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void get_allTeams_OK() throws Exception {
        List<Team> teams = Arrays.asList(createBasicTeam(), createBasicTeam(), createBasicTeam());
        String teamsJson = objectMapper.writeValueAsString(teams);
        when(teamService.findAll()).thenReturn(teams);

        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/teams"))
                .andExpect(status().isOk())
                .andExpect(content().json(teamsJson));
    }

    @Test
    public void get_existedTeam_OK() throws Exception {
        when(teamService.findOne(persistedTeam.getTeamId())).thenReturn(Optional.of(persistedTeam));

        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/teams/" + persistedTeam.getTeamId()))
                .andExpect(status().isOk())
                .andExpect(content().json(persistedTeamJson));
    }

    @Test
    public void get_notExistedTeam_NOT_FOUND() throws Exception {
        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/teams/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void delete_team_OK() throws Exception {
        mockMvc.perform(delete(Constants.REST_PATH_PREFIX + "/teams/1"))
                .andExpect(status().isOk());
    }
}
