package ua.in.gdekatka.gkdata.controller.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ua.in.gdekatka.gkdata.config.Constants;
import ua.in.gdekatka.gkdata.controller.rest.RulesController;
import ua.in.gdekatka.gkdata.model.Rules;
import ua.in.gdekatka.gkdata.service.RulesService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ua.in.gdekatka.gkdata.util.EntityCreator.createRules;

@RunWith(SpringRunner.class)
@WebMvcTest(RulesController.class)
public class RulesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private RulesService rulesService;

    private static Rules persistedRules;
    private static String notPersistedRulesJson;
    private static String persistedRulesJson;

    @Before
    public void setUp() throws Exception {
        Rules notPersistedRules = createRules();
        persistedRules = createRules();
        persistedRules.setRulesId(1L);

        notPersistedRulesJson = objectMapper.writeValueAsString(notPersistedRules);
        persistedRulesJson = objectMapper.writeValueAsString(persistedRules);
    }

    @Test
    public void post_validRules_OK() throws Exception {
        when(rulesService.save(any(Rules.class))).thenReturn(persistedRules);

        mockMvc.perform(post(Constants.REST_PATH_PREFIX + "/rules")
                .accept(MediaType.APPLICATION_JSON)
                .content(notPersistedRulesJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().json(persistedRulesJson));
    }

    @Test
    public void post_rulesWithId_BAD_REQUEST() throws Exception {
        mockMvc.perform(
                post(Constants.REST_PATH_PREFIX + "/rules")
                        .accept(MediaType.APPLICATION_JSON)
                        .content(persistedRulesJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void put_validRules_OK() throws Exception {
        when(rulesService.save(any(Rules.class))).thenReturn(persistedRules);

        mockMvc.perform(put(Constants.REST_PATH_PREFIX + "/rules/" + persistedRules.getRulesId())
                .accept(MediaType.APPLICATION_JSON)
                .content(persistedRulesJson)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(persistedRulesJson));
    }

    @Test
    public void put_rulesWithoutId_BAD_REQUEST() throws Exception {
        mockMvc.perform(
                put(Constants.REST_PATH_PREFIX + "/rules/" + persistedRules.getRulesId())
                        .accept(MediaType.APPLICATION_JSON)
                        .content(notPersistedRulesJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void get_allRules_OK() throws Exception {
        List<Rules> rules = Arrays.asList(createRules(), createRules(), createRules());
        String rulesJson = objectMapper.writeValueAsString(rules);
        when(rulesService.findAll()).thenReturn(rules);

        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/rules"))
                .andExpect(status().isOk())
                .andExpect(content().json(rulesJson));
    }

    @Test
    public void get_existedRules_OK() throws Exception {
        when(rulesService.findOne(persistedRules.getRulesId())).thenReturn(Optional.of(persistedRules));

        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/rules/" + persistedRules.getRulesId()))
                .andExpect(status().isOk())
                .andExpect(content().json(persistedRulesJson));
    }

    @Test
    public void get_notExistedRules_NOT_FOUND() throws Exception {
        mockMvc.perform(get(Constants.REST_PATH_PREFIX + "/rules/0"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void delete_rules_OK() throws Exception {
        mockMvc.perform(delete(Constants.REST_PATH_PREFIX + "/rules/1"))
                .andExpect(status().isOk());
    }
}
