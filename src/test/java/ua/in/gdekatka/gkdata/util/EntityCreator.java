package ua.in.gdekatka.gkdata.util;

import ua.in.gdekatka.gkdata.model.Award;
import ua.in.gdekatka.gkdata.model.Court;
import ua.in.gdekatka.gkdata.model.CourtImage;
import ua.in.gdekatka.gkdata.model.News;
import ua.in.gdekatka.gkdata.model.Player;
import ua.in.gdekatka.gkdata.model.Rules;
import ua.in.gdekatka.gkdata.model.Team;
import ua.in.gdekatka.gkdata.model.Tournament;
import ua.in.gdekatka.gkdata.model.embaddable.AwardCategoryEnum;
import ua.in.gdekatka.gkdata.model.embaddable.DrawEnum;
import ua.in.gdekatka.gkdata.model.embaddable.SystemFormatEnum;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;

import static ua.in.gdekatka.gkdata.util.TestConstants.TEST_AVATAR;
import static ua.in.gdekatka.gkdata.util.TestConstants.TEST_THUMBNAIL;

public class EntityCreator {

    public static Award createAward() {
        return Award.builder()
                .name("testName_" + System.currentTimeMillis())
                .avatar(TEST_AVATAR)
                .thumbnail(TEST_THUMBNAIL)
                .description("test description")
                .awardCategory(AwardCategoryEnum.TEAM)
                .build();
    }

    public static News createNews() {
        return News.builder()
                .title("testTitle")
                .content("test content")
                .avatar(TEST_AVATAR)
                .thumbnail(TEST_THUMBNAIL)
                .build();
    }

    public static Rules createRules() {
        return Rules.builder()
                .name("testName" + System.currentTimeMillis())
                .teamFormat(3)
                .timeFormat("1X10")
                .attackTime(12)
                .teamsLimit(0)
                .description("test description")
                .drawInfo(DrawEnum.AUTOMATED)
                .systemFormat(SystemFormatEnum.GROUP)
                .build();
    }

    public static CourtImage createCourtImage() {
        return CourtImage.builder()
                .link(TEST_AVATAR)
                .thumbnail(TEST_THUMBNAIL)
                .build();
    }

    public static Court createBasicCourt() {
        return Court.builder()
                .name("testName")
                .avatar(TEST_AVATAR)
                .thumbnail(TEST_THUMBNAIL)
                .latitude(11.111111)
                .longitude(22.222222)
                .rating(7)
                .description("test description")
                .images(new ArrayList<>())
                .build();
    }

    public static Player createBasicPlayer() {
        return Player.builder()
                .nickname("nName_" + System.currentTimeMillis())
                .email("test" + System.currentTimeMillis() + "@g.com")
                .avatar(TEST_AVATAR)
                .thumbnail(TEST_THUMBNAIL)
                .fname("testFName")
                .lname("testLName")
                .isMale(true)
                .dateOfBirth(LocalDate.now().minusYears(20))
                .lastSeen(LocalDateTime.now())
                .build();
    }

    public static Team createBasicTeam() {
        return Team.builder()
                .name("name" + System.currentTimeMillis())
                .avatar(TEST_AVATAR)
                .thumbnail(TEST_THUMBNAIL)
                .build();
    }

    public static Tournament createBasicTournament() {
        return Tournament.builder()
                .name("testName")
                .avatar(TEST_AVATAR)
                .thumbnail(TEST_THUMBNAIL)
                .date(LocalDateTime.now().plusDays(10))
                .place(createBasicCourt())
                .organizer(createBasicPlayer())
                .rules(createRules())
                .build();
    }
}
