package ua.in.gdekatka.gkdata.util;

/**
 * Test constants.
 */
public final class TestConstants {

    private TestConstants() {
    }

    //integration tests
    public static final String DOCKER_IMAGE_NAME = "postgres:10.4";
    public static final String DB_NAME = "gkdata";
    public static final String DB_USER_NAME = "gkdata";

    public static final String TEST_AVATAR = "test.avatar.link";
    public static final String TEST_THUMBNAIL = "test.thumbnail.link";
}
