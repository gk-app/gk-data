package ua.in.gdekatka.gkdata.integration;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;
import ua.in.gdekatka.gkdata.model.Player;
import ua.in.gdekatka.gkdata.repository.PlayerRepository;
import ua.in.gdekatka.gkdata.service.PlayerService;
import ua.in.gdekatka.gkdata.util.TestConstants;

import javax.persistence.EntityNotFoundException;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;
import static ua.in.gdekatka.gkdata.util.EntityCreator.createBasicPlayer;

@RunWith(SpringRunner.class)
// webEnvironment is set to workaround GraphQLWebAutoConfiguration creation issue
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = {PlayerPersistenceTests.Initializer.class})
@AutoConfigureTestDatabase(replace = NONE)
public class PlayerPersistenceTests {

    @Autowired
    private PlayerService playerService;
    @Autowired
    private PlayerRepository playerRepository;

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer(TestConstants.DOCKER_IMAGE_NAME)
            .withDatabaseName(TestConstants.DB_NAME)
            .withUsername(TestConstants.DB_USER_NAME);

    @Before
    public void cleanUp() {
        playerRepository.deleteAll();
    }

    @Test
    public void save_notPersistedPlayer_success() {
        Player playerToSave = createBasicPlayer();
        Player savedPlayer = playerService.save(playerToSave);

        assertNotEquals("Saved player does not have id",
                null, savedPlayer.getPlayerId());
    }

    @Test
    public void findOne_persistedPlayer_success() {
        Player playerToFind = playerService.save(createBasicPlayer());

        Player foundPlayer = playerService.findOne(playerToFind.getPlayerId())
                .orElseThrow(() -> new EntityNotFoundException("Player not found by id"));

        assertTrue("Found player is differ from expected",
                isNonEntityPropertiesEqual(playerToFind, foundPlayer));
    }

    @Test
    public void save_modifiedPersistedPlayer_success() {
        Player playerToModify = playerService.save(createBasicPlayer());
        playerToModify.setNickname("new nName");
        playerToModify.setAvatar("new.avatar.link");
        playerToModify.setThumbnail("new.thumbnail.link");
        playerToModify.setFname("new fName");
        playerToModify.setLname("new lName");
        playerToModify.setIsMale(false);
        playerToModify.setDateOfBirth(LocalDate.now().minusYears(20));

        Player modifiedPlayer = playerService.save(playerToModify);

        assertNotSame("Service did not return new entity after modification",
                playerToModify, modifiedPlayer);

        assertTrue("Modified player is differ from expected",
                isNonEntityPropertiesEqual(playerToModify, modifiedPlayer));
    }

    @Test
    public void delete_persistedPlayer_success() {
        Player playerToDelete = playerService.save(createBasicPlayer());

        playerService.delete(playerToDelete.getPlayerId());

        assertFalse("Player is found after its deletion",
                playerService.findOne(playerToDelete.getPlayerId()).isPresent());
    }

    @Test
    public void findAll_persistedPlayers_success() {
        int PERSISTED_ENTITIES_NUMBER = 6;

        for (int i = 0; i < PERSISTED_ENTITIES_NUMBER; i++) {
            playerService.save(createBasicPlayer());
        }

        assertEquals("Found number of entities does not correspond to expected",
                PERSISTED_ENTITIES_NUMBER, playerService.findAll().size());
    }

    private boolean isNonEntityPropertiesEqual(Player player1, Player player2) {
        return player1.getPlayerId().equals(player2.getPlayerId()) &&
                player1.getNickname().equals(player2.getNickname()) &&
                player1.getAvatar().equals(player2.getAvatar()) &&
                player1.getThumbnail().equals(player2.getThumbnail()) &&
                player1.getFname().equals(player2.getFname()) &&
                player1.getLname().equals(player2.getLname()) &&
                player1.getIsMale().equals(player2.getIsMale()) &&
                player1.getDateOfBirth().equals(player2.getDateOfBirth());
    }

    static class Initializer
            implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(@NotNull ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }
}
