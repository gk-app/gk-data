package ua.in.gdekatka.gkdata.integration;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;
import ua.in.gdekatka.gkdata.model.News;
import ua.in.gdekatka.gkdata.repository.NewsRepository;
import ua.in.gdekatka.gkdata.service.NewsService;
import ua.in.gdekatka.gkdata.util.TestConstants;

import javax.persistence.EntityNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;
import static ua.in.gdekatka.gkdata.util.EntityCreator.createNews;

@RunWith(SpringRunner.class)
// webEnvironment is set to workaround GraphQLWebAutoConfiguration creation issue
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = {NewsPersistenceTests.Initializer.class})
@AutoConfigureTestDatabase(replace = NONE)
public class NewsPersistenceTests {

    @Autowired
    private NewsService newsService;
    @Autowired
    private NewsRepository newsRepository;

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer(TestConstants.DOCKER_IMAGE_NAME)
            .withDatabaseName(TestConstants.DB_NAME)
            .withUsername(TestConstants.DB_USER_NAME);

    @Before
    public void cleanUp() {
        newsRepository.deleteAll();
    }

    @Test
    public void save_notPersistedNews_success() {
        News newsToSave = createNews();
        News savedNews = newsService.save(newsToSave);

        assertNotEquals("Saved news does not have id",
                null, savedNews.getNewsId());
    }

    @Test
    public void findOne_persistedNews_success() {
        News newsToFind = newsService.save(createNews());

        News foundNews = newsService.findOne(newsToFind.getNewsId())
                .orElseThrow(() -> new EntityNotFoundException("News not found by id"));

        newsToFind.setCreatedAt(foundNews.getCreatedAt());

        assertTrue("Found news is differ from expected",
                isNonEntityPropertiesEqual(newsToFind, foundNews));
    }

    @Test
    public void save_modifiedPersistedNews_success() {
        News newsToModify = newsService.save(createNews());
        newsToModify.setTitle("new title");
        newsToModify.setContent("new content");
        newsToModify.setAvatar("new.avatar.link");
        newsToModify.setThumbnail("new.thumbnail.link");

        News modifiedNews = newsService.save(newsToModify);

        assertNotSame("Service did not return new entity after modification",
                newsToModify, modifiedNews);

        newsToModify.setCreatedAt(modifiedNews.getCreatedAt());

        assertTrue("Modified news is differ from expected",
                isNonEntityPropertiesEqual(newsToModify, modifiedNews));
    }

    @Test
    public void delete_persistedNews_success() {
        News newsToDelete = newsService.save(createNews());

        newsService.delete(newsToDelete.getNewsId());

        assertFalse("News is found after its deletion",
                newsService.findOne(newsToDelete.getNewsId()).isPresent());
    }

    @Test
    public void findAll_persistedNews_success() {
        int PERSISTED_ENTITIES_NUMBER = 6;

        for (int i = 0; i < PERSISTED_ENTITIES_NUMBER; i++) {
            newsService.save(createNews());
        }

        assertEquals("Found number of entities does not correspond to expected",
                PERSISTED_ENTITIES_NUMBER, newsService.findAll().size());
    }

    private boolean isNonEntityPropertiesEqual(News news1, News news2) {
        return news1.getNewsId().equals(news2.getNewsId()) &&
                news1.getTitle().equals(news2.getTitle()) &&
                news1.getContent().equals(news2.getContent()) &&
                news1.getAvatar().equals(news2.getAvatar()) &&
                news1.getThumbnail().equals(news2.getThumbnail());
    }

    static class Initializer
            implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(@NotNull ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }
}
