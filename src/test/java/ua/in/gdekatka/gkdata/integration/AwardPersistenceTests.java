package ua.in.gdekatka.gkdata.integration;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;
import ua.in.gdekatka.gkdata.model.Award;
import ua.in.gdekatka.gkdata.model.embaddable.AwardCategoryEnum;
import ua.in.gdekatka.gkdata.repository.AwardRepository;
import ua.in.gdekatka.gkdata.service.AwardService;
import ua.in.gdekatka.gkdata.util.TestConstants;

import javax.persistence.EntityNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;
import static ua.in.gdekatka.gkdata.util.EntityCreator.createAward;

@RunWith(SpringRunner.class)
// webEnvironment is set to workaround GraphQLWebAutoConfiguration creation issue
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = {AwardPersistenceTests.Initializer.class})
@AutoConfigureTestDatabase(replace = NONE)
public class AwardPersistenceTests {

    @Autowired
    private AwardService awardService;
    @Autowired
    private AwardRepository awardRepository;

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer(TestConstants.DOCKER_IMAGE_NAME)
            .withDatabaseName(TestConstants.DB_NAME)
            .withUsername(TestConstants.DB_USER_NAME);

    @Before
    public void cleanUp() {
        awardRepository.deleteAll();
    }

    @Test
    public void save_notPersistedAward_success() {
        Award awardToSave = createAward();
        Award savedAward = awardService.save(awardToSave);

        assertNotEquals("Saved award does not have id",
                null, savedAward.getAwardId());
    }

    @Test
    public void findOne_persistedAward_success() {
        Award awardToFind = awardService.save(createAward());

        Award foundAward = awardService.findOne(awardToFind.getAwardId())
                .orElseThrow(() -> new EntityNotFoundException("Award not found by id"));

        assertTrue("Found award is differ from expected",
                isNonEntityPropertiesEqual(awardToFind, foundAward));
    }

    @Test
    public void save_modifiedPersistedAward_success() {
        Award awardToModify = awardService.save(createAward());
        awardToModify.setName("newName");
        awardToModify.setAvatar("new.avatar.link");
        awardToModify.setThumbnail("new.thumbnail.link");
        awardToModify.setDescription("new description");
        awardToModify.setAwardCategory(AwardCategoryEnum.PERSONAL);

        Award modifiedAward = awardService.save(awardToModify);

        assertNotSame("Service did not return new entity after modification",
                awardToModify, modifiedAward);

        assertTrue("Modified award is differ from expected",
                isNonEntityPropertiesEqual(awardToModify, modifiedAward));
    }

    @Test
    public void delete_persistedAward_success() {
        Award awardToDelete = awardService.save(createAward());

        awardService.delete(awardToDelete.getAwardId());

        assertFalse("Award is found after its deletion",
                awardService.findOne(awardToDelete.getAwardId()).isPresent());
    }

    @Test
    public void findAll_persistedAwards_success() {
        int PERSISTED_ENTITIES_NUMBER = 6;

        for (int i = 0; i < PERSISTED_ENTITIES_NUMBER; i++) {
            awardService.save(createAward());
        }

        assertEquals("Found number of entities does not correspond to expected",
                PERSISTED_ENTITIES_NUMBER, awardService.findAll().size());
    }

    private boolean isNonEntityPropertiesEqual(Award award1, Award award2) {
        return award1.getAwardId().equals(award2.getAwardId()) &&
                award1.getName().equals(award2.getName()) &&
                award1.getAvatar().equals(award2.getAvatar()) &&
                award1.getThumbnail().equals(award2.getThumbnail()) &&
                award1.getDescription().equals(award2.getDescription()) &&
                award1.getAwardCategory().equals(award2.getAwardCategory());
    }

    static class Initializer
            implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(@NotNull ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }
}
