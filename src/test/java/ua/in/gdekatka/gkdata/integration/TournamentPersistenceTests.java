package ua.in.gdekatka.gkdata.integration;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;
import ua.in.gdekatka.gkdata.model.Court;
import ua.in.gdekatka.gkdata.model.Player;
import ua.in.gdekatka.gkdata.model.Rules;
import ua.in.gdekatka.gkdata.model.Tournament;
import ua.in.gdekatka.gkdata.repository.CourtRepository;
import ua.in.gdekatka.gkdata.repository.PlayerRepository;
import ua.in.gdekatka.gkdata.repository.RulesRepository;
import ua.in.gdekatka.gkdata.repository.TournamentRepository;
import ua.in.gdekatka.gkdata.service.TournamentService;
import ua.in.gdekatka.gkdata.util.TestConstants;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;
import static ua.in.gdekatka.gkdata.util.EntityCreator.createBasicCourt;
import static ua.in.gdekatka.gkdata.util.EntityCreator.createBasicPlayer;
import static ua.in.gdekatka.gkdata.util.EntityCreator.createBasicTournament;
import static ua.in.gdekatka.gkdata.util.EntityCreator.createRules;

@RunWith(SpringRunner.class)
// webEnvironment is set to workaround GraphQLWebAutoConfiguration creation issue
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = {TournamentPersistenceTests.Initializer.class})
@AutoConfigureTestDatabase(replace = NONE)
public class TournamentPersistenceTests {

    @Autowired
    private TournamentService tournamentService;
    @Autowired
    private TournamentRepository tournamentRepository;
    @Autowired
    private CourtRepository courtRepository;
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private RulesRepository rulesRepository;

    private static Court court1;
    private static Player player1;
    private static Rules rules1;

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer(TestConstants.DOCKER_IMAGE_NAME)
            .withDatabaseName(TestConstants.DB_NAME)
            .withUsername(TestConstants.DB_USER_NAME);

    @Before
    public void cleanUp() {
        tournamentRepository.deleteAll();
        courtRepository.deleteAll();
        playerRepository.deleteAll();
        rulesRepository.deleteAll();

        court1 = courtRepository.save(createBasicCourt());
        player1 = playerRepository.save(createBasicPlayer());
        rules1 = rulesRepository.save(createRules());
    }

    @Test
    public void save_notPersistedTournament_success() {
        Tournament tournamentToSave = createBasicTournament();
        tournamentToSave.setPlace(court1);
        tournamentToSave.setOrganizer(player1);
        tournamentToSave.setRules(rules1);

        Tournament savedTournament = tournamentService.save(tournamentToSave);

        assertNotEquals("Saved tournament does not have id",
                null, savedTournament.getTournamentId());
    }

    @Test
    public void findOne_persistedTournament_success() {
        Tournament tournamentToSave = createBasicTournament();
        tournamentToSave.setPlace(court1);
        tournamentToSave.setOrganizer(player1);
        tournamentToSave.setRules(rules1);

        Tournament tournamentToFind = tournamentService.save(tournamentToSave);

        Tournament foundTournament = tournamentService.findOne(tournamentToFind.getTournamentId())
                .orElseThrow(() -> new EntityNotFoundException("Tournament not found by id"));

        assertTrue("Found tournament is differ from expected",
                isNonEntityPropertiesEqual(tournamentToFind, foundTournament));
    }

    @Test
    public void save_modifiedPersistedTournament_success() {
        Tournament tournamentToSave = createBasicTournament();
        tournamentToSave.setPlace(court1);
        tournamentToSave.setOrganizer(player1);
        tournamentToSave.setRules(rules1);

        Tournament tournamentToModify = tournamentService.save(tournamentToSave);
        tournamentToModify.setName("newName");
        tournamentToModify.setAvatar("new.avatar.link");
        tournamentToModify.setThumbnail("new.thumbnail.link");
        tournamentToModify.setDate(LocalDateTime.now().plusDays(11));

        Tournament modifiedTournament = tournamentService.save(tournamentToModify);

        assertNotSame("Service did not return new entity after modification",
                tournamentToModify, modifiedTournament);

        assertTrue("Modified tournament is differ from expected",
                isNonEntityPropertiesEqual(tournamentToModify, modifiedTournament));
    }

    @Test
    public void delete_persistedTournament_success() {
        Tournament tournamentToSave = createBasicTournament();
        tournamentToSave.setPlace(court1);
        tournamentToSave.setOrganizer(player1);
        tournamentToSave.setRules(rules1);

        Tournament tournamentToDelete = tournamentService.save(tournamentToSave);

        tournamentService.delete(tournamentToDelete.getTournamentId());

        assertFalse("Tournament is found after its deletion",
                tournamentService.findOne(tournamentToDelete.getTournamentId()).isPresent());
    }

    @Test
    public void findAll_persistedTournaments_success() {
        int PERSISTED_ENTITIES_NUMBER = 6;

        for (int i = 0; i < PERSISTED_ENTITIES_NUMBER; i++) {
            Tournament tournamentToSave = createBasicTournament();
            tournamentToSave.setPlace(courtRepository.save(createBasicCourt()));
            tournamentToSave.setOrganizer(playerRepository.save(createBasicPlayer()));
            tournamentToSave.setRules(rulesRepository.save(createRules()));
            tournamentService.save(tournamentToSave);
        }

        assertEquals("Found number of entities does not correspond to expected",
                PERSISTED_ENTITIES_NUMBER, tournamentService.findAll().size());
    }

    private boolean isNonEntityPropertiesEqual(Tournament tournament1, Tournament tournament2) {
        return tournament1.getTournamentId().equals(tournament2.getTournamentId()) &&
                tournament1.getName().equals(tournament2.getName()) &&
                tournament1.getAvatar().equals(tournament2.getAvatar()) &&
                tournament1.getThumbnail().equals(tournament2.getThumbnail()) &&
                tournament1.getDate().equals(tournament2.getDate());
    }

    static class Initializer
            implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        public void initialize(@NotNull ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }
}
