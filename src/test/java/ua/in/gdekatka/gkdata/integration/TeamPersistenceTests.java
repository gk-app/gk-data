package ua.in.gdekatka.gkdata.integration;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;
import ua.in.gdekatka.gkdata.model.Player;
import ua.in.gdekatka.gkdata.model.Team;
import ua.in.gdekatka.gkdata.repository.PlayerRepository;
import ua.in.gdekatka.gkdata.repository.TeamRepository;
import ua.in.gdekatka.gkdata.service.PlayerService;
import ua.in.gdekatka.gkdata.service.TeamService;
import ua.in.gdekatka.gkdata.util.TestConstants;

import javax.persistence.EntityNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;
import static ua.in.gdekatka.gkdata.util.EntityCreator.createBasicPlayer;
import static ua.in.gdekatka.gkdata.util.EntityCreator.createBasicTeam;

@RunWith(SpringRunner.class)
// webEnvironment is set to workaround GraphQLWebAutoConfiguration creation issue
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = {TeamPersistenceTests.Initializer.class})
@AutoConfigureTestDatabase(replace = NONE)
public class TeamPersistenceTests {

    @Autowired
    private TeamService teamService;
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private PlayerService playerService;
    @Autowired
    private PlayerRepository playerRepository;

    private static Player player1;

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer(TestConstants.DOCKER_IMAGE_NAME)
            .withDatabaseName(TestConstants.DB_NAME)
            .withUsername(TestConstants.DB_USER_NAME);

    @Before
    public void cleanUp() {
        teamRepository.deleteAll();
        playerRepository.deleteAll();

        player1 = playerService.save(createBasicPlayer());
    }

    @Test
    public void save_notPersistedTeam_success() {
        Team teamToSave = createBasicTeam();
        teamToSave.setCaptain(player1);

        Team savedTeam = teamService.save(teamToSave);

        assertNotEquals("Saved team does not have id",
                null, savedTeam.getTeamId());
    }

    @Test
    public void findOne_persistedTeam_success() {
        Team teamToSave = createBasicTeam();
        teamToSave.setCaptain(player1);

        Team teamToFind = teamService.save(teamToSave);

        Team foundTeam = teamService.findOne(teamToFind.getTeamId())
                .orElseThrow(() -> new EntityNotFoundException("Team not found by id"));

        assertTrue("Found team is differ from expected",
                isNonEntityPropertiesEqual(teamToFind, foundTeam));
    }

    @Test
    public void save_modifiedPersistedTeam_success() {
        Team teamToSave = createBasicTeam();
        teamToSave.setCaptain(player1);

        Team teamToModify = teamService.save(teamToSave);
        teamToModify.setName("newName");
        teamToModify.setAvatar("new.avatar.link");
        teamToModify.setThumbnail("new.thumbnail.link");

        Team modifiedTeam = teamService.save(teamToModify);

        assertNotSame("Service did not return new entity after modification",
                teamToModify, modifiedTeam);

        assertTrue("Modified team is differ from expected",
                isNonEntityPropertiesEqual(teamToModify, modifiedTeam));
    }

    @Test
    public void delete_persistedTeam_success() {
        Team teamToSave  = createBasicTeam();
        teamToSave.setCaptain(player1);

        Team teamToDelete = teamService.save(teamToSave);

        teamService.delete(teamToDelete.getTeamId());

        assertFalse("Team is found after its deletion",
                teamService.findOne(teamToDelete.getTeamId()).isPresent());
    }

    @Test
    public void findAll_persistedTeams_success() {
        int PERSISTED_ENTITIES_NUMBER = 6;

        for (int i = 0; i < PERSISTED_ENTITIES_NUMBER; i++) {
            Team teamToSave = createBasicTeam();
            teamToSave.setCaptain(playerService.save(createBasicPlayer()));
            teamService.save(createBasicTeam());
        }

        assertEquals("Found number of entities does not correspond to expected",
                PERSISTED_ENTITIES_NUMBER, teamService.findAll().size());
    }

    private boolean isNonEntityPropertiesEqual(Team team1, Team team2) {
        return team1.getTeamId().equals(team2.getTeamId()) &&
                team1.getName().equals(team2.getName()) &&
                team1.getAvatar().equals(team2.getAvatar()) &&
                team1.getThumbnail().equals(team2.getThumbnail());
    }

    static class Initializer
            implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(@NotNull ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }
}
