package ua.in.gdekatka.gkdata.integration;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;
import ua.in.gdekatka.gkdata.model.Court;
import ua.in.gdekatka.gkdata.model.CourtImage;
import ua.in.gdekatka.gkdata.repository.CourtRepository;
import ua.in.gdekatka.gkdata.service.CourtService;
import ua.in.gdekatka.gkdata.util.TestConstants;

import javax.persistence.EntityNotFoundException;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;
import static ua.in.gdekatka.gkdata.util.EntityCreator.createBasicCourt;
import static ua.in.gdekatka.gkdata.util.EntityCreator.createCourtImage;

@RunWith(SpringRunner.class)
// webEnvironment is set to workaround GraphQLWebAutoConfiguration creation issue
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = {CourtPersistenceTests.Initializer.class})
@AutoConfigureTestDatabase(replace = NONE)
public class CourtPersistenceTests {

    @Autowired
    private CourtService courtService;
    @Autowired
    private CourtRepository courtRepository;

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer(TestConstants.DOCKER_IMAGE_NAME)
            .withDatabaseName(TestConstants.DB_NAME)
            .withUsername(TestConstants.DB_USER_NAME);

    @Before
    public void cleanUp() {
        courtRepository.deleteAll();
    }

    @Test
    public void save_notPersistedCourt_success() {
        Court courtToSave = createBasicCourt();
        Court savedCourt = courtService.save(courtToSave);

        assertNotEquals("Saved court does not have id",
                null, savedCourt.getCourtId());
    }

    @Test
    public void findOne_persistedCourt_success() {
        Court courtToFind = courtService.save(createBasicCourt());

        Court foundCourt = courtService.findOne(courtToFind.getCourtId())
                .orElseThrow(() -> new EntityNotFoundException("Court not found by id"));

        assertTrue("Found court is differ from expected",
                isNonEntityPropertiesEqual(courtToFind, foundCourt));
    }

    @Test
    public void save_modifiedPersistedCourt_success() {
        Court courtToModify = courtService.save(createBasicCourt());
        courtToModify.setName("new name");
        courtToModify.setAvatar("new.avatar.link");
        courtToModify.setThumbnail("new.thumbnail.link");
        courtToModify.setLatitude(33.333333);
        courtToModify.setLongitude(44.444444);
        courtToModify.setRating(4);
        courtToModify.setDescription("new description");

        Court modifiedCourt = courtService.save(courtToModify);

        assertNotSame("Service did not return new entity after modification",
                courtToModify, modifiedCourt);

        assertTrue("Modified court is differ from expected",
                isNonEntityPropertiesEqual(courtToModify, modifiedCourt));
    }

    @Test
    public void delete_persistedCourt_success() {
        Court courtToDelete = courtService.save(createBasicCourt());

        courtService.delete(courtToDelete.getCourtId());

        assertFalse("Court is found after its deletion",
                courtService.findOne(courtToDelete.getCourtId()).isPresent());
    }

    @Test
    public void findAll_persistedCourts_success() {
        int PERSISTED_ENTITIES_NUMBER = 6;

        for (int i = 0; i < PERSISTED_ENTITIES_NUMBER; i++) {
            courtService.save(createBasicCourt());
        }

        assertEquals("Found number of entities does not correspond to expected",
                PERSISTED_ENTITIES_NUMBER, courtService.findAll().size());
    }

    @Test
    public void save_notPersistedCourtWithCourtImage_success() {
        Court courtToSave = createBasicCourt();
        courtToSave.getImages().add(createCourtImage());
        Court savedCourt = courtService.save(courtToSave);

        assertEquals("Court image was not saved or saved number of images not correspond with the expected",
                1, savedCourt.getImages().size());

        assertNotEquals("Saved court image does not have id",
                null, savedCourt.getImages().get(0));
    }

    @Test
    public void save_notPersistedCourtWithMultipleCourtImages_success() {
        Court courtToSave = createBasicCourt();
        courtToSave.getImages().addAll(Arrays.asList(createCourtImage(), createCourtImage(), createCourtImage()));
        Court savedCourt = courtService.save(courtToSave);

        assertEquals("Court images were not saved or saved number of images not correspond with the expected",
                3, savedCourt.getImages().size());

        assertTrue("Saved court images do not have ids",
                savedCourt.getImages().stream().allMatch(o -> o.getCourtImageId() != null));
    }

    @Test
    public void save_modifiedPersistedCourtImage_success() {
        Court courtToSave = createBasicCourt();
        courtToSave.getImages().add(createCourtImage());

        Court savedCourt = courtService.save(courtToSave);
        CourtImage savedCourtImage = savedCourt.getImages().get(0);

        savedCourtImage.setLink("new.court.image.link");
        savedCourtImage.setThumbnail("new.court.image.thumbnail");

        Court modifiedCourt = courtService.save(savedCourt);
        CourtImage modifiedCourtImage = modifiedCourt.getImages().get(0);

        assertNotSame("Service did not return new entity after modification",
                savedCourtImage, modifiedCourtImage);

        assertTrue("Modified court image is differ from expected",
                savedCourtImage.getLink().equals(modifiedCourtImage.getLink()) &&
                savedCourtImage.getThumbnail().equals(modifiedCourtImage.getThumbnail()));
    }

    @Test
    public void delete_persistedCourtImage_success() {
        Court courtToSave = createBasicCourt();
        courtToSave.getImages().addAll(Arrays.asList(createCourtImage(), createCourtImage(), createCourtImage()));

        Court savedCourt = courtService.save(courtToSave);
        savedCourt.getImages().remove(0);

        Court modifiedCourt = courtService.save(savedCourt);

        assertNotSame("Service did not return new entity after modification",
                savedCourt, modifiedCourt);

        assertEquals("After deletion number of images not correspond with the expected",
                2, modifiedCourt.getImages().size());

        modifiedCourt.getImages().clear();

        Court noImagesCourt = courtService.save(modifiedCourt);

        assertEquals("All images were not deleted from court",
                0, noImagesCourt.getImages().size());
    }

    private boolean isNonEntityPropertiesEqual(Court court1, Court court2) {
        return court1.getCourtId().equals(court2.getCourtId()) &&
                court1.getName().equals(court2.getName()) &&
                court1.getAvatar().equals(court2.getAvatar()) &&
                court1.getThumbnail().equals(court2.getThumbnail()) &&
                court1.getLatitude().equals(court2.getLatitude()) &&
                court1.getLongitude().equals(court2.getLongitude()) &&
                court1.getRating().equals(court2.getRating()) &&
                court1.getDescription().equals(court2.getDescription());
    }

    static class Initializer
            implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        public void initialize(@NotNull ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }
}
