package ua.in.gdekatka.gkdata.integration;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;
import ua.in.gdekatka.gkdata.model.Rules;
import ua.in.gdekatka.gkdata.model.embaddable.DrawEnum;
import ua.in.gdekatka.gkdata.model.embaddable.SystemFormatEnum;
import ua.in.gdekatka.gkdata.repository.RulesRepository;
import ua.in.gdekatka.gkdata.service.RulesService;
import ua.in.gdekatka.gkdata.util.TestConstants;

import javax.persistence.EntityNotFoundException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;
import static ua.in.gdekatka.gkdata.util.EntityCreator.createRules;

@RunWith(SpringRunner.class)
// webEnvironment is set to workaround GraphQLWebAutoConfiguration creation issue
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = {RulesPersistenceTests.Initializer.class})
@AutoConfigureTestDatabase(replace = NONE)
public class RulesPersistenceTests {

    @Autowired
    private RulesService rulesService;
    @Autowired
    private RulesRepository rulesRepository;

    @ClassRule
    public static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer(TestConstants.DOCKER_IMAGE_NAME)
            .withDatabaseName(TestConstants.DB_NAME)
            .withUsername(TestConstants.DB_USER_NAME);

    @Before
    public void cleanUp() {
        rulesRepository.deleteAll();
    }

    @Test
    public void save_notPersistedRules_success() {
        Rules rulesToSave = createRules();
        Rules savedRules = rulesService.save(rulesToSave);

        assertNotEquals("Saved rules does not have id",
                null, savedRules.getRulesId());
    }

    @Test
    public void findOne_persistedRules_success() {
        Rules rulesToFind = rulesService.save(createRules());

        Rules foundRules = rulesService.findOne(rulesToFind.getRulesId())
                .orElseThrow(() -> new EntityNotFoundException("Rules not found by id"));

        assertTrue("Found rules is differ from expected",
                isNonEntityPropertiesEqual(rulesToFind, foundRules));
    }

    @Test
    public void save_modifiedPersistedRules_success() {
        Rules rulesToModify = rulesService.save(createRules());
        rulesToModify.setName("new name");
        rulesToModify.setTeamFormat(5);
        rulesToModify.setTimeFormat("20X2");
        rulesToModify.setAttackTime(24);
        rulesToModify.setTeamsLimit(32);
        rulesToModify.setDescription("new description");
        rulesToModify.setDrawInfo(DrawEnum.MANUAL);
        rulesToModify.setSystemFormat(SystemFormatEnum.MIXED);

        Rules modifiedRules = rulesService.save(rulesToModify);

        assertNotSame("Service did not return new entity after modification",
                rulesToModify, modifiedRules);

        assertTrue("Modified rules is differ from expected",
                isNonEntityPropertiesEqual(rulesToModify, modifiedRules));
    }

    @Test
    public void delete_persistedRules_success() {
        Rules rulesToDelete = rulesService.save(createRules());

        rulesService.delete(rulesToDelete.getRulesId());

        assertFalse("Rules is found after its deletion",
                rulesService.findOne(rulesToDelete.getRulesId()).isPresent());
    }

    @Test
    public void findAll_persistedRules_success() {
        int PERSISTED_ENTITIES_NUMBER = 6;

        for (int i = 0; i < PERSISTED_ENTITIES_NUMBER; i++) {
            rulesService.save(createRules());
        }

        assertEquals("Found number of entities does not correspond to expected",
                PERSISTED_ENTITIES_NUMBER, rulesService.findAll().size());
    }

    private boolean isNonEntityPropertiesEqual(Rules rules1, Rules rules2) {
        return rules1.getRulesId().equals(rules2.getRulesId()) &&
                rules1.getName().equals(rules2.getName()) &&
                rules1.getTeamFormat().equals(rules2.getTeamFormat()) &&
                rules1.getTimeFormat().equals(rules2.getTimeFormat()) &&
                rules1.getAttackTime().equals(rules2.getAttackTime()) &&
                rules1.getTeamsLimit().equals(rules2.getTeamsLimit()) &&
                rules1.getDescription().equals(rules2.getDescription()) &&
                rules1.getDrawInfo().equals(rules2.getDrawInfo()) &&
                rules1.getSystemFormat().equals(rules2.getSystemFormat());
    }

    static class Initializer
            implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        public void initialize(@NotNull ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of(
                    "spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
                    "spring.datasource.username=" + postgreSQLContainer.getUsername(),
                    "spring.datasource.password=" + postgreSQLContainer.getPassword()
            ).applyTo(configurableApplicationContext.getEnvironment());
        }
    }
}
