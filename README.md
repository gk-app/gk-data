# GK-DATA
Persistence service for GK-APP project.

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing 
purposes.

### Prerequisites
Before you start you need to have installed

- Java SE Development Kit 11
- Maven
- Docker

### Installing

Start postgres container:
```
docker run --name postgres -p 5432:5432 -e POSTGRES_USER=gkdata -e POSTGRES_PASSWORD=gkdata -d postgres
```

In IDEA create Run Configuration with environment variables:
```
SPRING_ACTIVE_PROFILE=dev;
SPRING_DATASOURCE_URL=jdbc:postgresql://<postgres.container.ip>:5432/gkdata;
SPRING_DATASOURCE_USERNAME=gkdata;
SPRING_DATASOURCE_PASSWORD=gkdata;
PORT=<port.number>
```

Run the application.

## Running the tests
Project contains Unit and Integration tests

### Integration Tests
Integration tests are run in docker containers via Testcontainers library.
Tests can be run on linux machine with docker installed. 
Run tests in the same way you do to run unit test, simply right-click on desired test 
and chose Run from the context menu. 

## Deployment

TODO: Add additional notes about how to deploy the app on a live system

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management
