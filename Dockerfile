FROM java:8

WORKDIR /gkdata

COPY target/gk-data-0.0.1-SNAPSHOT.jar .

EXPOSE 8081

CMD java -jar gk-data-0.0.1-SNAPSHOT.jar
