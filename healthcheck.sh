#!/usr/bin/env bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

GKDATA_HEALTH_ENDPOINT=https://gkdata.herokuapp.com/actuator/health
EXPECTED_RESPONSE="{\"status\":\"UP\"}"

function writeLog() {
    echo -e $2 "${GREEN}$(date '+%Y-%m-%d %H:%M:%S'):${NC} $1";
}

function checkHealth() {
    response=$(curl -X GET -s ${GKDATA_HEALTH_ENDPOINT})

    if [[ ${response} == ${EXPECTED_RESPONSE} ]]; then
        echo -e "${GREEN}SUCCESS: ${NC} Application is up!"
        exit 0
    fi
}

writeLog "Performing requests to ${GKDATA_HEALTH_ENDPOINT} endpoint..."

while [[ $SECONDS -lt 60 ]]; do
    checkHealth
    sleep 5
done

echo -e "${RED}ERROR:${NC} Application was not up in 1 minutes."

exit 1
